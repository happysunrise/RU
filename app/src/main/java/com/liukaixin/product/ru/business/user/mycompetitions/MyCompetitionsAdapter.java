package com.liukaixin.product.ru.business.user.mycompetitions;

import android.content.Context;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.data.model.Competition;

/**
 * Created by Jacob on 2016/9/14.
 */
public class MyCompetitionsAdapter extends RecyclerArrayAdapter<Competition> {

    public MyCompetitionsAdapter(Context context){
        super(context);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyCompetitionsViewHolder(parent);
    }
}
