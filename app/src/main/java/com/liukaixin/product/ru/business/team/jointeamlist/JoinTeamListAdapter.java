package com.liukaixin.product.ru.business.team.jointeamlist;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.data.model.Team;

/**
 * Created by Jacob on 2016/9/18.
 */
public class JoinTeamListAdapter extends RecyclerArrayAdapter<Team>{
public JoinTeamListAdapter(Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new JoinTeamListViewHolder(parent);
    }
}
