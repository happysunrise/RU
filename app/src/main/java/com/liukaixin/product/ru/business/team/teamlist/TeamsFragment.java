package com.liukaixin.product.ru.business.team.teamlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.LazyLoadFragment;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.team.teamdesc.TeamDescActivity;
import com.liukaixin.product.ru.data.model.Team;
import com.orhanobut.logger.Logger;

import org.w3c.dom.Text;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 团队列表视图层.
 *
 * Created by liukaixin on 16/8/29.
 */
public class TeamsFragment extends LazyLoadFragment
        implements TeamsContract.View,
        RecyclerArrayAdapter.OnLoadMoreListener,SwipeRefreshLayout.OnRefreshListener  {

    private TeamsContract.Presenter presenter;

    private TeamsAdapter teamsAdapter;
    private EasyRecyclerView recyclerView;

    private Integer page = 1;

    public TeamsFragment(){
        // Requires empty public constructor
    }

    public static TeamsFragment newInstance(){
        return new TeamsFragment();
    }

    @Override
    public void setPresenter(TeamsContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        teamsAdapter = new TeamsAdapter(getActivity());
        //Create the presenter
        presenter = new TeamsPresenter(Injection
                .provideTeamsRepository(),this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teams,container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //set up the team view
        recyclerView = (EasyRecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapterWithProgress(teamsAdapter);
        recyclerView.setRefreshListener(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        teamsAdapter.setMore(R.layout.view_more, this);
        teamsAdapter.setNoMore(R.layout.view_no_more);
        teamsAdapter.setOnItemClickListener(
                this::toTeamDescActivity);
        teamsAdapter.setError(R.layout.view_error)
                .setOnClickListener(
                v -> teamsAdapter.resumeMore());
    }

    private void toTeamDescActivity(Integer position) {
        Intent toTeamDesc = new Intent(getContext(), TeamDescActivity.class);
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_TEAM_ID,
                teamsAdapter.getItem(position).getId());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_TEAM_NAME,
                teamsAdapter.getItem(position).getName());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_CREATOR_ID,
                teamsAdapter.getItem(position).getUserId());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_COMPETITION_NAME,
                teamsAdapter.getItem(position).getCompetitionName());
        startActivity(toTeamDesc);
    }

    @Override
    public void initData() {
        onRefresh();
    }

    @Override
    public void onRefresh() {
        Logger.i("刷新团队数据");
        presenter.refresh();
    }

    @Override
    public void onLoadMore() {
        page++;
        Logger.d("加载第%d页竞赛数据", page);
        presenter.loadMore(page);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(List<Team> teams) {
        recyclerView.setVisibility(View.VISIBLE);
        teamsAdapter.addAll(teams);
    }

    @Override
    public void showNoData() {
        recyclerView.showEmpty();
        teamsAdapter.clear();
    }

    @Override
    public void clearAdapter() {
        teamsAdapter.clear();
    }

    @Override
    public void initPage() {
        page = 1;
    }

    @Override
    public void setNoMoreData() {
        teamsAdapter.stopMore();
    }

}
