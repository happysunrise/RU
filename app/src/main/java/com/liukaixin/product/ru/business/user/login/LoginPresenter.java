package com.liukaixin.product.ru.business.user.login;

import android.support.annotation.NonNull;

import com.igexin.sdk.PushManager;
import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.repositories.UserRepository;
import com.liukaixin.product.ru.util.CheckUtil;
import com.liukaixin.product.ru.util.EncryptUtil;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/3.
 */

public class LoginPresenter implements LoginContract.Presenter {

    @NonNull
    private final UserRepository userRepository;

    @NonNull
    private final LoginContract.View loginView;

    public LoginPresenter(@NonNull UserRepository newUserRepository,
                          @NonNull LoginContract.View newLoginView) {

        this.userRepository = checkNotNull(newUserRepository);
        this.loginView = checkNotNull(newLoginView);

        this.loginView.setPresenter(this);
    }

    @Override
    public void login() {
        loginView.showLoading();
        String username = loginView.getUsername();
        String password = loginView.getPassword();

        // 检查用户名是否为空.
        if (username.isEmpty()) {
            loginView.hideLoading();
            loginView.showUsernameEmpty();
            return;
        }

        // 检查密码是否为空.
        if (password.isEmpty()) {
            loginView.hideLoading();
            loginView.showPasswordEmpty();
            return;
        }

        // 检查密码是否是字母和数字的组合且在8到16位.
        if (!CheckUtil.isAAndDBetween8to16(password)) {
            loginView.hideLoading();
            loginView.showPasswordFormatDismatch();
            return;
        }

        // 检查邮箱是否格式正确.
        if (!CheckUtil.isCorrectEmail(username)) {
            loginView.hideLoading();
            loginView.showInvalidMail();
            return;
        }

        // 检查是否有网络,如果没有弹窗提示
        if (CheckUtil.haveNetWork(loginView.getContext())) {
            User user = new User(username, EncryptUtil.encrypt(password));

            userRepository.login(user, new CallBackListener<User>() {
                @Override
                public void onCompleted() {
                    loginView.hideLoading();
                }

                @Override
                public void onError(String msg) {
                    loginView.showError(msg);
                }

                @Override
                public void onNext(User user) {
                    loginView.toLoginSuccessActivity();
                    addClientId(PushManager.getInstance()
                            .getClientid(loginView.getContext().getApplicationContext()));
                }
            });
        }
    }

    private void addClientId(String clientId) {
        userRepository.addClientId(clientId, new CallBackListener<Integer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(String msg) {

            }

            @Override
            public void onNext(Integer integer) {

            }
        });
    }
}
