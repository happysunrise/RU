package com.liukaixin.product.ru.business.team.check;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Apply;
import com.liukaixin.product.ru.enumerate.ApplyStatusEnum;
import com.liukaixin.product.ru.util.LVCircularSmile;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Jacob on 2016/9/21.
 */
public class CheckFragment extends Fragment implements CheckContract.View,
        View.OnClickListener{

    @NonNull
    private static final String ARGUMENT_APPLY_ID = "APPLY_ID";

    private CheckContract.Presenter presenter;

    private CoordinatorLayout wholeLayout;

    private TextView teamNameText;
    private TextView nameText;
    private TextView schoolText;
    private TextView degreeText;
    private TextView abilityText;
    private TextView contactText;

    private FloatingActionButton agreeFab;
    private FloatingActionButton disagreeFab;
    private FloatingActionButton haveAgreeFab;
    private FloatingActionButton haveDisagreeFab;

    private CircleImageView profileImg;

    private LinearLayout checkLayout;
    private LinearLayout haveCheckLayout;

    private LVCircularSmile smileLoading;

    private Apply apply;

    public CheckFragment() {

    }

    public static CheckFragment newInstance(Integer applyId) {
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_APPLY_ID, applyId);
        CheckFragment fragment = new CheckFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    public void setPresenter(@NonNull CheckContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_check,container,false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        wholeLayout = (CoordinatorLayout) view.findViewById(R.id.whole_layout);
        wholeLayout.setVisibility(View.GONE);
        teamNameText = (TextView) view.findViewById(R.id.team_name_text);
        // 人名
        nameText = (TextView) view.findViewById(R.id.name_text);
        schoolText = (TextView) view.findViewById(R.id.school_text);
        degreeText = (TextView) view.findViewById(R.id.degree_text);
        abilityText = (TextView) view.findViewById(R.id.ability_text);
        contactText = (TextView) view.findViewById(R.id.contact_text);

        agreeFab = (FloatingActionButton) view.findViewById(R.id.agree_fab);
        agreeFab.setOnClickListener(this);
        disagreeFab = (FloatingActionButton) view.findViewById(R.id.disagree_fab);
        disagreeFab.setOnClickListener(this);
        haveAgreeFab = (FloatingActionButton) view.findViewById(R.id.have_agree_fab);
        haveAgreeFab.setOnClickListener(this);
        haveDisagreeFab = (FloatingActionButton) view.findViewById(R.id.have_reject_fab);
        haveDisagreeFab.setOnClickListener(this);
        smileLoading = (LVCircularSmile) view.findViewById(R.id.lv_circularSmile);

        checkLayout = (LinearLayout) view.findViewById(R.id.check_layout);
        haveCheckLayout = (LinearLayout) view.findViewById(R.id.have_check_layout);
        profileImg = (CircleImageView) view.findViewById(R.id.profile_image);
    }

    @Override
    public void onResume() {
        super.onResume();
        //根据id获取apply
        presenter.getApplyById();
    }
    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), "加载出错，请返回重试", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData() {
        wholeLayout.setVisibility(View.VISIBLE);
        teamNameText.setText(apply.getTeamName());
        nameText.setText(apply.getApplyUserNickName());
        schoolText.setText(apply.getApplyUserSchool());
        degreeText.setText(apply.getApplyUserDegree());
        abilityText.setText(apply.getAbility());
        contactText.setText(apply.getApplyUserContact());
        Glide.with(getContext())
                .load(apply.getApplyUserPic())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(profileImg);
    }

    @Override
    public void showLoading() {
        smileLoading.setVisibility(View.VISIBLE);
        smileLoading.startAnim();
    }

    @Override
    public void hideLoading() {
        smileLoading.setVisibility(View.GONE);
        smileLoading.stopAnim();
    }

    @Override
    public void setApply(Apply result) {
        this.apply = result;
    }

    @Override
    public void showNotCheckView() {
        checkLayout.setVisibility(View.VISIBLE);
        haveCheckLayout.setVisibility(View.GONE);
    }

    @Override
    public void showAgreeView() {
        checkLayout.setVisibility(View.GONE);
        haveCheckLayout.setVisibility(View.VISIBLE);
        haveAgreeFab.setVisibility(View.VISIBLE);
        haveDisagreeFab.setVisibility(View.GONE);
    }

    @Override
    public void showRejectView() {
        checkLayout.setVisibility(View.GONE);
        haveCheckLayout.setVisibility(View.VISIBLE);
        haveDisagreeFab.setVisibility(View.VISIBLE);
        haveAgreeFab.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.agree_fab:
                presenter.getOtherApplyStatus(apply.getApplyUserId(),
                        apply.getCompetitionName(), ApplyStatusEnum.AGREE.getTypeCode());
                break;
            case R.id.disagree_fab:
                presenter.getOtherApplyStatus(apply.getApplyUserId(),
                        apply.getCompetitionName(), ApplyStatusEnum.REJECT.getTypeCode());
                break;
            case R.id.have_agree_fab:
                Toast.makeText(getContext(), "您已同意该申请", Toast.LENGTH_SHORT).show();
                break;
            case R.id.have_reject_fab:
                Toast.makeText(getContext(), "您已拒绝该申请", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }
}
