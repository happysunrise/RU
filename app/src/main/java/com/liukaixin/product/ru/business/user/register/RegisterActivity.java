package com.liukaixin.product.ru.business.user.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.user.login.LoginActivity;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * 注册.
 * <p>
 * Created by liukaixin on 16/9/7.
 */

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        RegisterFragment registerFragment = (RegisterFragment) getSupportFragmentManager()
                .findFragmentById(R.id.content_frame);

        if (registerFragment == null) {
            registerFragment = RegisterFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    registerFragment, R.id.content_frame);
        }

        Log.i("not create fragment", "register");
        new RegisterPresenter(Injection.provideUserRepository(),
                registerFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        onBackPressed();
        Intent toLogin = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(toLogin);
        return true;
    }

}
