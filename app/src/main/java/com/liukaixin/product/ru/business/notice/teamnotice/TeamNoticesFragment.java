package com.liukaixin.product.ru.business.notice.teamnotice;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.LazyLoadFragment;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.team.check.CheckActivity;
import com.liukaixin.product.ru.data.model.Notice;
import com.liukaixin.product.ru.enumerate.NoticeTypeEnum;
import com.orhanobut.logger.Logger;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 2016/10/19.
 */

public class TeamNoticesFragment extends LazyLoadFragment
        implements TeamNoticesContract.View,
        RecyclerArrayAdapter.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    private TeamNoticesContract.Presenter presenter;

    private TeamNoticesAdapter noticesAdapter;

    private Integer page = 1;

    private EasyRecyclerView recyclerView;

    public TeamNoticesFragment() {

    }

    public static TeamNoticesFragment newInstance() {
        return new TeamNoticesFragment();
    }

    @Override
    public void setPresenter(TeamNoticesContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NotificationManager manager = (NotificationManager)
                getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancelAll();

        noticesAdapter = new TeamNoticesAdapter(getActivity());
        // Create the presenter
        presenter = new TeamNoticesPresenter(Injection
                .provideNoticeRepository(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_team_notice, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set up the competitions view
        recyclerView = (EasyRecyclerView) view.findViewById(R.id.recycler_view_team);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapterWithProgress(noticesAdapter);
        recyclerView.setRefreshListener(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        noticesAdapter.setMore(R.layout.view_more, this);
        noticesAdapter.setNoMore(R.layout.view_no_more);
        noticesAdapter.setOnItemClickListener(position -> {
            Intent toCheck = new Intent(getContext(), CheckActivity.class);
            toCheck.putExtra(CheckActivity.EXTRA_APPLY_ID,
                    noticesAdapter.getItem(position).getIdForCategory());
            startActivity(toCheck);
        });
        noticesAdapter.setError(R.layout.view_error)
                .setOnClickListener(
                        v -> noticesAdapter.resumeMore());
    }

    @Override
    public void onRefresh() {
        presenter.refresh(NoticeTypeEnum.TEAM_NOTICE.getTypeCode());
    }

    @Override
    public void onLoadMore() {
        page++;
        Logger.d("加载第%d页团队申请数据", page);
        presenter.loadMore(page, NoticeTypeEnum.TEAM_NOTICE.getTypeCode());
    }

    @Override
    public void initData() {
        onRefresh();
    }

    @Override
    public void initPage() {
        page = 1;
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNoData() {
        recyclerView.showEmpty();
    }

    @Override
    public void clearAdapter() {
        noticesAdapter.clear();
    }

    @Override
    public void setNoMoreData() {
        noticesAdapter.stopMore();
    }

    @Override
    public void showData(List<Notice> notice) {
        noticesAdapter.addAll(notice);
    }
}
