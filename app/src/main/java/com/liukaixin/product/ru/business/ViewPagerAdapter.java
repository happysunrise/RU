package com.liukaixin.product.ru.business;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by liukaixin on 16/8/29.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private String[] titles;
    private List<Fragment> fragments;

    public ViewPagerAdapter(FragmentManager supportFragmentManager,
                            String[] titles, List<Fragment> fragments) {
        super(supportFragmentManager);
        this.titles = titles;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
