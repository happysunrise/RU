package com.liukaixin.product.ru.business.team.teamdesc;

import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.TeamMember;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by liukaixin on 16/8/30.
 */
public class TeamMembersViewHolder extends BaseViewHolder<TeamMember> {

    private CircleImageView picImg;
    private TextView nameText;


    public TeamMembersViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_team_member);
        nameText = $(R.id.name_text);
        picImg = $(R.id.profile_image);
    }

    @Override
    public void setData(final TeamMember teamMember){
        nameText.setText(teamMember.getNickName());
        Glide.with(getContext())
                .load(teamMember.getUserPic())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(picImg);
    }
}
