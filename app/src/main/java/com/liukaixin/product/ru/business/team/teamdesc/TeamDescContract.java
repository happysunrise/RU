package com.liukaixin.product.ru.business.team.teamdesc;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Apply;
import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.data.model.Team;
import com.liukaixin.product.ru.data.model.TeamMember;

import java.util.List;

/**
 * Created by Jacob on 2016/9/19.
 */
public interface TeamDescContract {

    interface View extends BaseView<Presenter> {

        void showErrorToast(String msg);

        void showData(Team team);

        void showLoading();

        void hideLoading();

        void showScrollView();


        void makeFavorited();

        void makeUnFavorited();

        void haveAgreeByOthers();

        void checkPersonalInfo();


        void showPlusTab(Integer typeCode);

        void setTeam(Team team);

        void getTeamMembers();

        void showNoData();

        void showTeamMembers(List<TeamMember> teamMembers);
    }

    interface Presenter extends BasePresenter {

        void getTeamDescById();

        void getApplyStatus(Integer applyUserId);

        void sendApply(Apply apply);

        void getOtherApplyStatus(Integer id, String string);

        void getFavoriteStatus(Integer userId);

        void whoseTeam(Integer creatorId, Integer userId);

        void doFavorite(Favorite favorite);

        void cancelFavorite(Integer userId);

        void getTeamMembers(Integer teamId);
    }
}
