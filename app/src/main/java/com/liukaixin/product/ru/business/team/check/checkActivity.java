package com.liukaixin.product.ru.business.team.check;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * Created by Jacob on 2016/9/21.
 */
public class CheckActivity extends AppCompatActivity {
    public static final String EXTRA_APPLY_ID = "APPLY_ID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        Integer applyId = getIntent().getIntExtra(EXTRA_APPLY_ID, 1);

        CheckFragment checkFragment = (CheckFragment) getSupportFragmentManager()
                .findFragmentById(R.id.content_frame);

        if(checkFragment == null) {
            checkFragment = CheckFragment.newInstance(applyId);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    checkFragment,R.id.content_frame);
        }

        // Create the presenter
        new CheckPresenter(applyId, Injection.provideTeamsRepository(), checkFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
