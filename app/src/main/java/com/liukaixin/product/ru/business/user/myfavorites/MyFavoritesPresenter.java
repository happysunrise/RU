package com.liukaixin.product.ru.business.user.myfavorites;

import android.os.Handler;
import android.support.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.liukaixin.product.ru.AppController;
import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.repositories.MyFavoritesRepository;
import com.liukaixin.product.ru.util.ACache;
import com.liukaixin.product.ru.util.CheckUtil;
import com.orhanobut.logger.Logger;

import java.util.List;

/**
 * Created by Jacob on 2016/9/16.
 */
public class MyFavoritesPresenter implements MyFavoritesContract.Presenter {

    @NonNull
    private final MyFavoritesRepository myFavoritesRepository;

    @NonNull
    private final MyFavoritesContract.View myFavoritesView;

    private Handler handler = new Handler();

    public MyFavoritesPresenter(@NonNull MyFavoritesRepository myFavoritesRepository,
                                @NonNull MyFavoritesContract.View myFavoritesView) {
        this.myFavoritesRepository = myFavoritesRepository;
        this.myFavoritesView = myFavoritesView;

        this.myFavoritesView.setPresenter(this);
    }

    @Override
    public void refresh() {
        myFavoritesView.initPage();
        // 获取第一页的十条数据
        getFavoritesByUserId(1, 10);
    }

    @Override
    public void loadMore(Integer page) {
        getFavoritesByUserId(page, 10);
    }

    private void getFavoritesByUserId(final Integer page,
                                      final Integer rows) {
        if (CheckUtil.haveNetWork(myFavoritesView.getContext())) {
            myFavoritesRepository.getFavoritesByUserId(page, rows,
                    new CallBackListener<List<Favorite>>() {

                        @Override
                        public void onCompleted() {
                            // do nothing
                        }

                        @Override
                        public void onError(String msg) {
                            myFavoritesView.showError("获取收藏失败");
                        }

                        @Override
                        public void onNext(List<Favorite> myFavorites) {
                            showDataOnView(myFavorites, page);
                        }
                    });
        }
    }

    private void showDataOnView(final List<Favorite> myFavorites,
                                final Integer page) {
        handler.postDelayed(() -> {
            if (page == 1 && myFavorites.isEmpty()) {
                myFavoritesView.showNoData();
                return;
            }
            if (page == 1) {
                myFavoritesView.clearAdapter();
            }
            if (myFavorites.size() % 10 > 0
                    || (page != 1 && myFavorites.isEmpty())) {
                myFavoritesView.setNoMoreData();
            }
            myFavoritesView.showData(myFavorites);
        }, 1000);
    }
}
