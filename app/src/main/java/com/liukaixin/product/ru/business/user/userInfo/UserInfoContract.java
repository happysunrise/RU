package com.liukaixin.product.ru.business.user.userInfo;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.User;

/**
 * Created by liukaixin on 16/9/20.
 */

public interface UserInfoContract {
    interface View extends BaseView<Presenter> {
        void showError(String msg);

        void showData(User user);

        void showLoading();

        void hideLoading();
    }

    interface Presenter extends BasePresenter {
        void getUserInfoById(Integer userId);
    }
}
