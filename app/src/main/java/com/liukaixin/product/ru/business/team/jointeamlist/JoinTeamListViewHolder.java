package com.liukaixin.product.ru.business.team.jointeamlist;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Team;

/**
 * Created by Jacob on 2016/9/18.
 */
public class JoinTeamListViewHolder extends BaseViewHolder<Team>{

    private TextView teamNameText;

    private TextView schoolText;

    private TextView requireText;

//    private ImageView fullImage;

    private ImageView teamCreatorPicImg;

    private TextView userNameText;

    private TextView numberText;


    public JoinTeamListViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_join_team_list);
        teamNameText = $(R.id.team_name_text);
        schoolText = $(R.id.school_text);
        requireText = $(R.id.require_text);
//        fullImage = $(R.id.full_img);
        teamCreatorPicImg = $(R.id.team_creator_pic_img);
        userNameText = $(R.id.user_name_text);
        numberText = $(R.id.number_text);
    }

    @Override
    public void setData(Team team) {
        teamNameText.setText(team.getName());
        schoolText.setText(team.getSchool());
        requireText.setText(team.getRequire());
        userNameText.setText(team.getUserNickName());
        numberText.setText("差" + String.valueOf(team.getNumber()) + "人");
        Glide.with(getContext())
                .load(team.getPic())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(teamCreatorPicImg);
    }
}
