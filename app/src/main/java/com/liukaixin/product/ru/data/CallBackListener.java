package com.liukaixin.product.ru.data;

/**
 * 数据获取情况的回调。
 *
 * Created by liukaixin on 16/9/1.
 */
public interface CallBackListener<T> {

    void onCompleted();

    void onError(String msg);

    void onNext(T t);

}
