package com.liukaixin.product.ru.manager;

import com.liukaixin.product.ru.enumerate.CompetitionTypeEnum;
import com.liukaixin.product.ru.enumerate.JoinWayEnum;

/**
 * Created by liukaixin on 16/8/31.
 */

public class CompetitionManager {
    public static String handleTypesIds(String typeIds) {
        if (typeIds.equals(CompetitionTypeEnum.PROJECT.getTypeCode())) {
            return CompetitionTypeEnum.PROJECT.getDescription();
        }
        if (typeIds.equals(CompetitionTypeEnum.STARTUPS.getTypeCode())) {
            return CompetitionTypeEnum.STARTUPS.getDescription();
        }
        if (typeIds.equals(CompetitionTypeEnum.IT_SCIENCE.getTypeCode())) {
            return CompetitionTypeEnum.IT_SCIENCE.getDescription();
        }
        if (typeIds.equals(CompetitionTypeEnum.ART.getTypeCode())) {
            return CompetitionTypeEnum.ART.getDescription();
        }
        if (typeIds.equals(CompetitionTypeEnum.ELSE.getTypeCode())) {
            return CompetitionTypeEnum.ELSE.getDescription();
        }
        return CompetitionTypeEnum.ALL.getDescription();
    }

    public static String handleJoinWay(Integer joinWay) {
        if (joinWay.equals(JoinWayEnum.TEAM.getTypeCode())) {
           return JoinWayEnum.TEAM.getDescription();
        }
        return JoinWayEnum.SINGLE.getDescription();
    }
}
