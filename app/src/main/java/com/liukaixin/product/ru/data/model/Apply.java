package com.liukaixin.product.ru.data.model;

/**
 * Created by liukaixin on 16/9/19.
 */

public class Apply {

    /**
     * 能力.
     */
    private String ability;

    /**
     * 申请人id.
     */
    private Integer applyUserId;

    /**
     * 团队id.
     */
    private Integer teamId;

    /**
     * 团队名称.
     */
    private String teamName;

    /**
     * 团队创始人id.
     */
    private Integer teamCreatorId;

    /**
     * 创始时间.
     */
    private String createTime;

    /**
     * 审核状态.
     */
    private Integer status;

    // 数据库没有但是程序中需要的的字段
    private String applyUserName;

    private String applyUserSchool;

    private String applyUserDegree;
    /**
     *  比赛名称.
     */
    private String competitionName;

    private String applyUserPic;

    private String applyUserNickName;

    private String applyUserContact;


    public Apply() {
        // mybatis need an empty constructor
    }

    public Apply(String ability, Integer applyUserId,
                 Integer teamId, Integer teamCreatorId) {
        this.ability = ability;
        this.applyUserId = applyUserId;
        this.teamId = teamId;
        this.teamCreatorId = teamCreatorId;
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public Integer getApplyUserId() {
        return applyUserId;
    }

    public void setApplyUserId(Integer applyUserId) {
        this.applyUserId = applyUserId;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getTeamCreatorId() {
        return teamCreatorId;
    }

    public void setTeamCreatorId(Integer teamCreatorId) {
        this.teamCreatorId = teamCreatorId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Apply{" +
                "ability='" + ability + '\'' +
                ", applyUserId=" + applyUserId +
                ", teamId=" + teamId +
                ", teamCreatorId=" + teamCreatorId +
                ", createTime='" + createTime + '\'' +
                '}';
    }

    public String getApplyUserRealName() {
        return applyUserName;
    }

    public void setApplyUserName(String applyUserName) {
        this.applyUserName = applyUserName;
    }

    public String getApplyUserSchool() {
        return applyUserSchool;
    }

    public void setApplyUserSchool(String applyUserSchool) {
        this.applyUserSchool = applyUserSchool;
    }

    public String getApplyUserDegree() {
        return applyUserDegree;
    }

    public void setApplyUserDegree(String applyUserDegree) {
        this.applyUserDegree = applyUserDegree;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getApplyUserName() {
        return applyUserName;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public String getApplyUserPic() {
        return applyUserPic;
    }

    public void setApplyUserPic(String applyUserPic) {
        this.applyUserPic = applyUserPic;
    }

    public String getApplyUserNickName() {
        return applyUserNickName;
    }

    public void setApplyUserNickName(String applyUserNickName) {
        this.applyUserNickName = applyUserNickName;
    }

    public String getApplyUserContact() {
        return applyUserContact;
    }

    public void setApplyUserContact(String applyUserContact) {
        this.applyUserContact = applyUserContact;
    }
}
