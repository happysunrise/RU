package com.liukaixin.product.ru.data.model;

/**
 * Created by Jacob on 2016/9/20.
 */
public class Notice {

    /**
     * 数据表中主键.
     */
    private Integer id;

    /**
     * 在每一类中的id，比如团队通知，则是加入申请的id.
     */
    private Integer idForCategory;

    /**
     *  通知题目.
     */
    private String title;

    /**
     * 通知分类. 1：竞赛通知  2：团队通知  3：系统通知
     */
    private Integer category;

    /**
     *  通知详情.
     */
    private String info;

    /**
     *  通知发布时间.
     */
    private String createTime;

    /**
     * 被通知者的id
     */
    private Integer userId;

    /**
     * 是否读过，0：未读，1：已读
     */
    private Integer read;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdForCategory() {
        return idForCategory;
    }

    public void setIdForCategory(Integer idForCategory) {
        this.idForCategory = idForCategory;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRead() {
        return read;
    }

    public void setRead(Integer read) {
        this.read = read;
    }
}
