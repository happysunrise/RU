package com.liukaixin.product.ru.business.team.createteam;

import android.support.annotation.Nullable;

import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Team;
import com.liukaixin.product.ru.data.model.UserTeam;
import com.liukaixin.product.ru.data.repositories.TeamsRepository;
import com.liukaixin.product.ru.enumerate.TeamRoleTypeEnum;
import com.liukaixin.product.ru.util.CheckUtil;
import com.orhanobut.logger.Logger;

/**
 * Created by Jacob on 2016/9/20.
 */
public class CreateTeamPresenter implements CreateTeamContract.Presenter {

    private final TeamsRepository teamsRepository;

    private final CreateTeamContract.View createTeamView;

    @Nullable
    private Integer competitionId;

    public CreateTeamPresenter(Integer competitionId, TeamsRepository teamsRepository,
                               CreateTeamContract.View createTeamView) {
        this.competitionId = competitionId;
        this.teamsRepository = teamsRepository;
        this.createTeamView = createTeamView;
        this.createTeamView.setPresenter(this);
    }

    @Override
    public void sendCreatedTeam(Team team) {
        if (CheckUtil.haveNetWork(createTeamView.getContext())) {
            Logger.d("要创建的团队是:%s", team.toString());
            createTeamView.showLoading();
            teamsRepository.sendCreatedTeam(team,
                    new CallBackListener<String>() {

                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(String msg) {
                            createTeamView.hideLoading();
                            createTeamView.showError(msg);
                        }

                        @Override
                        public void onNext(String s) {
                            createTeamView.hideLoading();
                            createTeamView.toSuccessCreatedTeam(s);
                        }
                    });
        }
    }

}
