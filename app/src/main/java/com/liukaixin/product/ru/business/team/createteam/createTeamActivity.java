package com.liukaixin.product.ru.business.team.createteam;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * Created by Jacob on 2016/9/20.
 */
public class CreateTeamActivity extends AppCompatActivity{
    public static final String EXTRA_COMPETITION_ID = "COMPETITION_ID";
    public static final String EXTRA_COMPETITION_NAME = "COMPETITION_NAME";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);

        // Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        // Get the requested competition id
        Integer competitionId = getIntent().getIntExtra(EXTRA_COMPETITION_ID, 1);

        // Get the competition name
        String competitionName = getIntent().getStringExtra(EXTRA_COMPETITION_NAME);

        CreateTeamFragment createTeamFragment = (CreateTeamFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if(createTeamFragment == null) {
            createTeamFragment = CreateTeamFragment.newInstance(competitionId,
                    competitionName);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    createTeamFragment, R.id.content_frame);
        }

        // Create the presenter
        new CreateTeamPresenter(competitionId,
                Injection.provideTeamsRepository(), createTeamFragment);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
