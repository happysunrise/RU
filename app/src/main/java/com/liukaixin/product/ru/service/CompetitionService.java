package com.liukaixin.product.ru.service;

import com.alibaba.fastjson.JSONObject;
import com.liukaixin.product.ru.data.model.Competition;
import com.liukaixin.product.ru.data.model.Result;

import java.util.List;
import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Completable;
import rx.Observable;

/**
 * Created by liukaixin on 16/8/31.
 */

public interface CompetitionService {

    /**
     * 获取发布状态的竞赛.
     *
     * @param page 页码
     * @param rows 条数
     * @return 结果集
     */
    @GET("competition/getReleasedCompetitions/{page}/{rows}")
    Observable<Result<List<Competition>>> getReleasedCompetitions(
            @Path("page") Integer page,
            @Path("rows") Integer rows);

    /**
     * 根据竞赛类型获取发布状态的竞赛.
     *
     * @param page 页码
     * @param rows 条数
     * @param type 竞赛类型
     * @return 结果集
     */
    @GET("competition/getReleasedCompetitionsByType/{page}/{rows}/{type}")
    Observable<Result<List<Competition>>> getReleasedCompetitionsByType(
            @Path("page") Integer page,
            @Path("rows") Integer rows,
            @Path("type") String type);

    /**
     * 根据用户id获取竞赛.
     *
     * @param page   页码
     * @param roes   条数
     * @param userId 用户id
     * @return 结果集
     */
    @GET("competition/getCompetitionsByUserId/{page}/{rows}/{userId}")
    Observable<Result<List<Competition>>> getCompetitionByUserId(
            @Path("page") Integer page,
            @Path("rows") Integer roes,
            @Path("userId") Integer userId);

    /**
     * 根据竞赛id获取竞赛详情.
     *
     * @param id 竞赛id
     * @return 结果集
     */
    @GET("competition/getCompetitionDesc/{id}")
    Observable<Result<Competition>> getCompetitionDescById(
            @Path("id") Integer id);

}
