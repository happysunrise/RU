package com.liukaixin.product.ru.business.user.mycompetitions;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Competition;
import com.orhanobut.logger.Logger;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Jacob on 2016/9/14.
 */
public class MyCompetitionsFragment extends Fragment implements MyCompetitionsContract.View,
        RecyclerArrayAdapter.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener{

    private MyCompetitionsContract.Presenter presenter;

    private MyCompetitionsAdapter myCompetitionsAdapter;

    private Integer page = 1;

    public MyCompetitionsFragment() {
        // Requires empty public constructor
    }

    public static MyCompetitionsFragment newInstance() {
        return new MyCompetitionsFragment();
    }

    @Override
    public void setPresenter(MyCompetitionsContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_competitions,container,false);
        myCompetitionsAdapter = new MyCompetitionsAdapter(getActivity());
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set up my competitions view
        EasyRecyclerView recyclerView =
                (EasyRecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapterWithProgress(myCompetitionsAdapter);
        recyclerView.setRefreshListener(this);

        myCompetitionsAdapter.setMore(R.layout.view_more,this);
        myCompetitionsAdapter.setNoMore(R.layout.view_no_more);
        myCompetitionsAdapter.setOnItemClickListener(
                position -> toCompetitionActivity());

        myCompetitionsAdapter.setError(R.layout.view_error)
                .setOnClickListener(
                        v -> myCompetitionsAdapter.resumeMore());

        onRefresh();
    }

    private void toCompetitionActivity() {
        Toast.makeText(getActivity(),"点击后转入竞赛详情页",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        Logger.d("onRefresh流程");
        presenter.refresh();
    }

    @Override
    public void onLoadMore() {
        Logger.d("onLoadMore流程");
        page++;
        presenter.loadMore(page);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(List<Competition> competitions) {
        myCompetitionsAdapter.addAll(competitions);
    }

    @Override
    public void showNoData() {
        myCompetitionsAdapter.clear();
    }

    @Override
    public void clearAdapter() {
        myCompetitionsAdapter.clear();
    }

    @Override
    public void initPage() {
        page = 1;
    }

    @Override
    public void setNoMoreData() {
        myCompetitionsAdapter.stopMore();
    }


}
