package com.liukaixin.product.ru.business.team.jointeamlist;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Team;

import java.util.List;

/**
 * Created by Jacob on 2016/9/18.
 */
public class JoinTeamListContract {
    public interface View extends BaseView<Presenter> {
        void showNoData();

        void clearAdapter();

        void setNoMoreData();

        void showData(List<Team> teams);

        void showError(String msg);

        void initPage();
    }

    public interface Presenter extends BasePresenter {
        void refresh();

        void loadMore(Integer page);
    }
}
