package com.liukaixin.product.ru.enumerate;

/**
 * Created by liukaixin on 16/9/23.
 */

public enum FavoriteTypeEnum {

    COMPETITION(1, "竞赛"),
    TEAM(0, "团队");

    private final Integer typeCode;
    private final String description;

    FavoriteTypeEnum(Integer typeCode, String description) {
        this.typeCode = typeCode;
        this.description = description;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public String getDescription() {
        return description;
    }

}
