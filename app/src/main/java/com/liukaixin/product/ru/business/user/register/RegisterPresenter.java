package com.liukaixin.product.ru.business.user.register;

import android.support.annotation.NonNull;

import com.igexin.sdk.PushManager;
import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.repositories.UserRepository;
import com.liukaixin.product.ru.util.CheckUtil;
import com.liukaixin.product.ru.util.EncryptUtil;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/7.
 */

public class RegisterPresenter implements RegisterContract.Presenter {

    @NonNull
    private final UserRepository userRepository;

    @NonNull
    private final RegisterContract.View registerView;


    public RegisterPresenter(@NonNull UserRepository userRepository,
                             @NonNull RegisterContract.View registerView) {
        this.userRepository = checkNotNull(userRepository);
        this.registerView = checkNotNull(registerView);

        this.registerView.setPresenter(this);
    }

    @Override
    public void register() {
        registerView.showLoading();
        String username = registerView.getUsername();
        if (username.isEmpty()) {
            registerView.hideLoading();
            registerView.emptyUsername();
            return;
        }
        String password = registerView.getPassword();
        if (password.isEmpty()) {
            registerView.hideLoading();
            registerView.emptyPassword();
            return;
        }
        String school = registerView.getSchool();
        if (school.isEmpty()) {
            registerView.hideLoading();
            registerView.emptySchool();
            return;
        }
        String degree = registerView.getDegree();
        if (!CheckUtil.isCorrectEmail(username)) {
            registerView.hideLoading();
            registerView.userNameFormatDismatch();
            return;
        }
        if (!CheckUtil.isAAndDBetween8to16(password)) {
            registerView.hideLoading();
            registerView.pwdNotCorrected();
            return;
        }
        if (CheckUtil.haveNetWork(registerView.getActivity())) {
            User user = new User(username, EncryptUtil.encrypt(password), school, degree);
            userRepository.register(user, new CallBackListener<String>() {
                @Override
                public void onCompleted() {
                    registerView.hideLoading();
                }

                @Override
                public void onError(String msg) {
                    registerView.showError(msg);
                }

                @Override
                public void onNext(String msg) {
                    addClientId(PushManager.getInstance()
                            .getClientid(registerView.getActivity().getApplicationContext()));
                    registerView.showRegisterSuccessActivity(msg);
                }
            });
        }
    }

    private void addClientId(String clientId) {
        userRepository.addClientId(clientId, new CallBackListener<Integer>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(String msg) {

            }

            @Override
            public void onNext(Integer integer) {

            }
        });
    }
}
