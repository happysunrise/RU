package com.liukaixin.product.ru.business.user.login;

import android.content.Context;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;

/**
 *  * This specifies the contract between the view and the presenter.
 *
 * Created by liukaixin on 16/9/3.
 */

public class LoginContract {

    interface View extends BaseView<Presenter> {

        String getUsername();

        String getPassword();

        void showError(String msg);

        void toLoginSuccessActivity();

        void showUsernameEmpty();

        void showPasswordEmpty();

        void showInvalidMail();

        void showLoading();

        void hideLoading();

        void showPasswordFormatDismatch();

        Context getContext();
    }

    interface Presenter extends BasePresenter {

        void login();

    }
}
