package com.liukaixin.product.ru.data.model;

/**
 * Created by liukaixin on 16/10/6.
 */

public class UserPush {

    private Integer userId;

    private String clientId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
