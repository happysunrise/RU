package com.liukaixin.product.ru.business.user.myapplies;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Apply;

import java.util.List;

/**
 * Created by liukaixin on 16/10/5.
 */

public interface MyAppliesContract {

    interface View extends BaseView<Presenter> {
        void initPage();

        void showError(String msg);

        void showNoData();

        void clearAdapter();

        void setNoMoreData();

        void showData(List<Apply> applies);
    }

    interface Presenter extends BasePresenter {

        void refresh();

        void loadMore(Integer page);
    }

}
