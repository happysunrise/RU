package com.liukaixin.product.ru.business.team.teammember;

import android.support.annotation.NonNull;

import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.repositories.TeamsRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/19.
 */

public class TeamMemberPresenter
        implements TeamMemberContract.Presenter {

    private final TeamsRepository teamsRepository;

    private final TeamMemberContract.View teamMemberView;

    public TeamMemberPresenter(@NonNull TeamsRepository teamsRepository,
                               @NonNull TeamMemberFragment teamMemberFragment) {
        this.teamsRepository = checkNotNull(teamsRepository);
        this.teamMemberView = checkNotNull(teamMemberFragment);
        this.teamMemberView.setPresenter(this);
    }

    @Override
    public void getMemberInfoByTeamIdAndUserId(Integer teamId, Integer userId) {
        teamMemberView.showLoading();
        teamsRepository.getTeamMemberInfoByTeamIdAndUserId(teamId, userId,
                new CallBackListener<User>() {
                    @Override
                    public void onCompleted() {
                        teamMemberView.hideLoading();
                    }

                    @Override
                    public void onError(String msg) {
                        teamMemberView.showError(msg);
                    }

                    @Override
                    public void onNext(User user) {
                        teamMemberView.showData(user);
                    }
                });
    }
}
