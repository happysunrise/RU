package com.liukaixin.product.ru;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.igexin.sdk.PushConsts;
import com.liukaixin.product.ru.business.notice.NoticesActivity;

/**
 * Created by liukaixin on 16/10/6.
 */

public class PushReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();

        switch (bundle.getInt(PushConsts.CMD_ACTION)) {
            case PushConsts.GET_CLIENTID:

                String cid = bundle.getString("clientid");
                // TODO:处理cid返回
                break;
            case PushConsts.GET_MSG_DATA:

//                String taskid = bundle.getString("taskid");
//                String messageid = bundle.getString("messageid");
//                byte[] payload = bundle.getByteArray("payload");
//                if (payload != null) {
//                    String data = new String(payload);

                    Intent toNotices = new Intent(context, NoticesActivity.class);
                    toNotices.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(toNotices);
//                }
                break;
            default:
                break;
        }
    }

}
