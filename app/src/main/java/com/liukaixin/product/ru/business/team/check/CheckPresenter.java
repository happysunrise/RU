package com.liukaixin.product.ru.business.team.check;

import android.support.annotation.NonNull;

import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Apply;
import com.liukaixin.product.ru.data.repositories.TeamsRepository;
import com.liukaixin.product.ru.enumerate.ApplyStatusEnum;
import com.orhanobut.logger.Logger;

/**
 * Created by Jacob on 2016/9/21.
 */
public class CheckPresenter implements CheckContract.Presenter {
    @NonNull
    private final TeamsRepository teamsRepository;

    @NonNull
    private final CheckContract.View checkView;

    private Integer applyId;

    public CheckPresenter(Integer applyId, @NonNull TeamsRepository teamsRepository,
                          @NonNull CheckContract.View checkView) {
        this.applyId = applyId;
        this.teamsRepository = teamsRepository;
        this.checkView = checkView;

        this.checkView.setPresenter(this);
    }

    /**
     * 获取申请信息.
     * <p>
     * 流程:
     * (1)显示加载层
     * (2)加载完成后隐藏加载层
     * (3)出错后显示出错层
     * (4)加载成功显示数据层.
     */
    @Override
    public void getApplyById() {
        checkView.showLoading();
        Logger.d("获取apply详情， apply id = %d", applyId);
        teamsRepository.getApplyById(applyId, new CallBackListener<Apply>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(String msg) {
                checkView.hideLoading();
                checkView.showError(msg);
            }

            @Override
            public void onNext(Apply result) {
                checkView.hideLoading();
                checkView.setApply(result);
                checkView.showData();
                if (result.getStatus().equals(ApplyStatusEnum.NOT_CHECK.getTypeCode())) {
                    checkView.showNotCheckView();
                } else if (result.getStatus().equals(ApplyStatusEnum.AGREE.getTypeCode())) {
                    checkView.showAgreeView();
                } else {
                    checkView.showRejectView();
                }
            }
        });
    }

    @Override
    public void checkApply(Integer status) {
        checkView.showLoading();
        Logger.d("处理加入申请apply id = %d, status = %d(2:agree,3:reject)", applyId, status);
        teamsRepository.updateApplyStatus(applyId, status,
                new CallBackListener<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(String msg) {
                        checkView.hideLoading();
                        checkView.showError(msg);
                    }

                    @Override
                    public void onNext(String s) {
                        checkView.hideLoading();
                        if (status.equals(ApplyStatusEnum.AGREE.getTypeCode())) {
                            checkView.showAgreeView();
                        } else {
                            checkView.showRejectView();
                        }
                    }
                });
    }

    /**
     * 获得其他团队的申请状态,如果该用户已被其他团队录用,则不能再申请新团队.
     * <p>
     * 流程:
     * (1)显示加载,按钮禁止点击,加载完成后隐藏加载
     * (2)加载错误,什么也不做
     * (3)加载成功,让申请加入按钮可点击
     * 如果被其他团队加入,则提示不能再申请该团队,并将已被加入设置为1,防止重复请求
     * 如果没有,则判断个人信息是否完整
     *
     * @param applyUserId     申请人的id(存在本机的id)
     * @param competitionName 申请的比赛的名称
     * @param typeCode        同意／拒绝
     */
    @Override
    public void getOtherApplyStatus(Integer applyUserId,
                                    String competitionName, Integer typeCode) {
        checkView.showLoading();
        Logger.d("判断该成员是否被其他团队录用，申请人id = %d，比赛名称 = %s",
                applyUserId, competitionName);
        teamsRepository.getOtherApplyStatus(applyUserId, competitionName,
                new CallBackListener<Integer>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(String msg) {
                        checkView.hideLoading();
                        checkView.showError(msg);
                    }

                    @Override
                    public void onNext(Integer status) {
                        if (!status.equals(ApplyStatusEnum.AGREE.getTypeCode())) {
                            checkApply(typeCode);
                        } else {
                            checkView.hideLoading();
                            checkView.showError("手慢了，该成员已被其他团队录用");
                        }
                    }
                });
    }
}
