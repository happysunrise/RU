package com.liukaixin.product.ru.business.notice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.ViewPagerAdapter;
import com.liukaixin.product.ru.business.competition.competitionlist.CompetitionsFragment;
import com.liukaixin.product.ru.business.notice.competitionnotice.CompetitionNoticesFragment;
import com.liukaixin.product.ru.business.notice.systemnotice.SystemNoticesFragment;
import com.liukaixin.product.ru.business.notice.teamnotice.TeamNoticesFragment;
import com.liukaixin.product.ru.business.team.teamlist.TeamsFragment;
import com.liukaixin.product.ru.util.ActivityUtils;

import java.util.ArrayList;
import java.util.List;

import static android.support.design.widget.TabLayout.MODE_FIXED;

/**
 * Created by Jacob on 2016/9/20.
 */
public class NoticesActivity extends AppCompatActivity{

    // tab栏
    private TabLayout tabLayout;
    // view pager
    private ViewPager viewPager;
    // TabLayout中的tab标题
    private String[] titles;
    // 填充到ViewPager中的Fragment
    private List<Fragment> fragments;

    public static final String EXTRA_CATEGORY_ID = "CATEGORY_ID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notices);

        // setToolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        // 初始化mTitles、mFragments等ViewPager需要的数据
        titles = getResources().getStringArray(R.array.notice_tab_titles);

        fragments = new ArrayList<>();
        TeamNoticesFragment teamNoticesFragment =
                TeamNoticesFragment.newInstance();
        fragments.add(teamNoticesFragment);

        CompetitionNoticesFragment competitionNoticesFragment =
                CompetitionNoticesFragment.newInstance();
        fragments.add(competitionNoticesFragment);

        SystemNoticesFragment systemNoticesFragment =
                SystemNoticesFragment.newInstance();
        fragments.add(systemNoticesFragment);

        // 初始化ViewPager的适配器，并设置给它
        ViewPagerAdapter viewPagerAdapter =
                new ViewPagerAdapter(getSupportFragmentManager(), titles, fragments);

        viewPager.setAdapter(viewPagerAdapter);
        // 设置ViewPager最大缓存的页面个数
        viewPager.setOffscreenPageLimit(3);

        tabLayout.setTabMode(MODE_FIXED);
        // 将TabLayout和ViewPager进行关联，让两者联动起来
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
