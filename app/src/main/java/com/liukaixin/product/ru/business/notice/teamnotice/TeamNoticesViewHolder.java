package com.liukaixin.product.ru.business.notice.teamnotice;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Notice;
import com.liukaixin.product.ru.enumerate.ApplyStatusEnum;

/**
 * Created by Jacob on 2016/9/20.
 */
public class TeamNoticesViewHolder extends BaseViewHolder<Notice> {

    private TextView titleText;
    private TextView infoText;
    private TextView createTimeText;

    private LinearLayout bg;

    public TeamNoticesViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_team_notice);
        titleText = $(R.id.title_text);
        infoText = $(R.id.info_text);
        createTimeText = $(R.id.create_time_text);

        bg = $(R.id.bg);
    }

    @Override
    public void setData(Notice notice) {
        titleText.setText(notice.getTitle());
        infoText.setText(notice.getInfo());
        createTimeText.setText(notice.getCreateTime());
        if (notice.getRead().equals(ApplyStatusEnum.NOT_CHECK.getTypeCode())) {
            bg.setBackgroundColor(0x7fffffff);
        } else {
            bg.setBackgroundColor(0x7fadaeb1);
        }
    }
}
