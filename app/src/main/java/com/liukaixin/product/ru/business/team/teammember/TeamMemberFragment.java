package com.liukaixin.product.ru.business.team.teammember;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.User;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/19.
 */

public class TeamMemberFragment extends Fragment
        implements TeamMemberContract.View {

    private TeamMemberContract.Presenter presenter;

    private TextView nickNameText;
    private TextView schoolText;
    private TextView majorText;
    private TextView degreeText;
    private TextView abilityText;

    public TeamMemberFragment() {
        // Requires empty public constructor
    }

    public static TeamMemberFragment newInstance() {
        return new TeamMemberFragment();
    }

    @Override
    public void setPresenter(@NonNull TeamMemberContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_team_member,
                container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);

        presenter.getMemberInfoByTeamIdAndUserId(12, 1);
    }

    private void initViews(View view) {
        nickNameText = (TextView) view.findViewById(R.id.nick_name_text);
        schoolText = (TextView) view.findViewById(R.id.school_text);
        majorText = (TextView) view.findViewById(R.id.major_text);
        degreeText = (TextView) view.findViewById(R.id.degree_text);
        abilityText = (TextView) view.findViewById(R.id.ability_text);
    }


    @Override
    public void showData(@NonNull User user) {
        checkNotNull(user);
        nickNameText.setText(user.getNickName());
        schoolText.setText(user.getSchool());
        majorText.setText(user.getMajor());
        degreeText.setText(user.getDegree());
        abilityText.setText(user.getAbility());
    }

    @Override
    public void showLoading() {
        Toast.makeText(getActivity(), "loading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoading() {
        Toast.makeText(getActivity(), "hide loading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }
}
