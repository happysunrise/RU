package com.liukaixin.product.ru.util;

/**
 * Created by liukaixin on 16/9/3.
 */

public class BusinessException extends RuntimeException {
    public BusinessException() {}

    public BusinessException(String message) {
        super(message);
    }
}
