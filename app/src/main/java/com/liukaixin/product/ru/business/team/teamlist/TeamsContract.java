package com.liukaixin.product.ru.business.team.teamlist;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Team;

import java.util.List;

/**
 * 团队列表约定.
 *
 * Created by Jacob on 2016/9/5.
 */
public interface TeamsContract {

    interface View extends BaseView<Presenter>{

        void showError(String msg);
        void showData(List<Team> teams);
        void showNoData();
        void clearAdapter();
        void initPage();
        void setNoMoreData();

    }

    interface Presenter extends BasePresenter{

        void getReleasedTeams(Integer page,Integer rows);
        void refresh();
        void loadMore(Integer page);

    }
}
