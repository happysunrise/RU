package com.liukaixin.product.ru.data.model;

/**
 * 学生用户.
 *
 * Created by liukaixin on 16/9/2.
 */
public class User {

    private Integer id;

    /**
     * 用户名，邮箱.
     */
    private String username;

    private String password;

    private String pic = "";

    private String school = "";

    /**
     * 0:非学生  1:学生.
     */
    private Integer is_student;

    private String degree;

    /**
     * 感兴趣的类型，格式1，4，5……
     */
    private String interestIds = "";

    /**
     * 擅长的领域，做成员推荐使用，格式1，3，4……
     */
    private String expertIds = "";

    /**
     * 0:禁止    1:活动.
     */
    private Integer status;
    private String nickName = "";
    private String major = "";
    private String ability = "";
    private String realName = "";

    private String gender = "";
    private String token;

    public User() {
    }

    public User(String username, String password, String school, String degree) {
        this.username = username;
        this.password = password;
        this.school = school;
        this.degree = degree;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getIs_student() {
        return is_student;
    }

    public void setIs_student(Integer is_student) {
        this.is_student = is_student;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public void setInterestIds(String interestIds) {
        this.interestIds = interestIds;
    }

    public String getInterestIds() {
        return interestIds;
    }

    public String getExpertIds() {
        return expertIds;
    }

    public void setExpertIds(String expertIds) {
        this.expertIds = expertIds;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNickName() {
        return nickName;
    }

    public String getMajor() {
        return major;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public String getAbility() {
        return ability;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", pic='" + pic + '\'' +
                ", school='" + school + '\'' +
                ", is_student=" + is_student +
                ", degree='" + degree + '\'' +
                ", interestIds='" + interestIds + '\'' +
                ", expertIds='" + expertIds + '\'' +
                ", status=" + status +
                ", nickName='" + nickName + '\'' +
                ", major='" + major + '\'' +
                ", ability='" + ability + '\'' +
                ", realName='" + realName + '\'' +
                ", gender='" + gender + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
