package com.liukaixin.product.ru;

import com.liukaixin.product.ru.data.repositories.CompetitionsRepository;
import com.liukaixin.product.ru.data.repositories.MyFavoritesRepository;
import com.liukaixin.product.ru.data.repositories.NoticeRepository;
import com.liukaixin.product.ru.data.repositories.TeamsRepository;
import com.liukaixin.product.ru.data.repositories.UserRepository;

/**
 * created by liukaixin.
 */
public class Injection {

    public static
    CompetitionsRepository provideCompetitionsRepository() {
        return CompetitionsRepository.getInstance();
    }

    public static
    UserRepository provideUserRepository() {
        return UserRepository.getInstance();
    }

    public static
    TeamsRepository provideTeamsRepository() {
        return TeamsRepository.getInstance();
    }


    public static
    MyFavoritesRepository provideMyFavoritesRepository() {
        return MyFavoritesRepository.getInstance();
    }

    public static
    NoticeRepository provideNoticeRepository() {
        return NoticeRepository.getInstance();
    }
}