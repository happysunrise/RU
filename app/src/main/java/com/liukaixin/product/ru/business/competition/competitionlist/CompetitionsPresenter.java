package com.liukaixin.product.ru.business.competition.competitionlist;

import android.os.Handler;
import android.support.annotation.NonNull;

import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Competition;
import com.liukaixin.product.ru.data.repositories.CompetitionsRepository;
import com.liukaixin.product.ru.enumerate.CompetitionTypeEnum;
import com.liukaixin.product.ru.util.CheckUtil;
import com.orhanobut.logger.Logger;

import java.io.Serializable;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * 比赛列表的业务逻辑.
 * <p>
 * Created by liukaixin on 16/8/29.
 */

public class CompetitionsPresenter
        implements CompetitionsContract.Presenter {

    // 数据仓库
    private final CompetitionsRepository competitionsRepository;

    // 视图层
    private final CompetitionsContract.View competitionsView;

    private CompetitionTypeEnum competitionTypeEnum =
            CompetitionTypeEnum.ALL;

    private Handler handler = new Handler();

    public CompetitionsPresenter(
            @NonNull CompetitionsRepository competitionsRepository,
            @NonNull CompetitionsContract.View competitionsView) {
        this.competitionsRepository =
                checkNotNull(competitionsRepository,
                        "competitionsRepository can not be null");
        this.competitionsView = checkNotNull(competitionsView,
                "competitionsView can not be null");

        this.competitionsView.setPresenter(this);
    }

    @Override
    public void refresh() {
        // 将fragment中的page置为1
        competitionsView.initPage();
        // 获取第一页的十条数据
        getData(1);
    }

    /**
     * 根据条件从数据仓库中获取发布状态打竞赛.
     *
     * @param page 页码
     * @param rows 页数
     * @param type 类型
     */
    @Override
    public void getReleasedCompetitionsByType(Integer page,
                                              Integer rows,
                                              String type) {
        if (CheckUtil.haveNetWork(competitionsView.getContext())) {
            competitionsRepository.getReleasedCompetitionsByType(page,
                    rows, type, new CallBackListener<List<Competition>>() {
                        @Override
                        public void onCompleted() {
                            // do nothing
                        }

                        @Override
                        public void onError(String msg) {
                            competitionsView.showError(msg);
                        }

                        @Override
                        public void onNext(List<Competition> competitions) {
                            Logger.d("按%s筛选第%d页竞赛", getFiltering().getDescription(), page);
                            showDataOnView(competitions, page);
                        }
                    });
        }
    }

    /**
     * 从数据仓库中获取发布状态的竞赛.
     *
     * @param page 页码
     * @param rows 条数
     */
    @Override
    public void getReleasedCompetitions(Integer page, Integer rows) {
        if (CheckUtil.haveNetWork(competitionsView.getContext())) {
            competitionsRepository.getReleasedCompetitions(page, rows,
                    new CallBackListener<List<Competition>>() {
                        @Override
                        public void onCompleted() {
                            // do nothing
                        }

                        @Override
                        public void onError(String msg) {
                            competitionsView.showError(msg);
                        }

                        @Override
                        public void onNext(List<Competition> competitions) {
                            showDataOnView(competitions, page);
                        }
                    });
        }
    }

    /**
     * 加载更多.
     *
     * @param page 页码
     */
    @Override
    public void loadMore(Integer page) {
        getData(page);
    }

    @Override
    public void setFiltering(CompetitionTypeEnum competitionTypeEnum) {
        this.competitionTypeEnum = competitionTypeEnum;
    }

    @Override
    public CompetitionTypeEnum getFiltering() {
        return competitionTypeEnum;
    }

    /**
     * 根据取数情况判断view如何显示.
     *
     * @param competitions 竞赛列表
     * @param page         页码
     */
    private void showDataOnView(List<Competition> competitions, Integer page) {
        handler.postDelayed(() -> {
            if (page == 1 && competitions.isEmpty()) {
                competitionsView.showNoData();
                return;
            }
            if (page == 1) {
                competitionsView.clearAdapter();
            }
            if (competitions.size() % 10 > 0 || (page != 1 && competitions.isEmpty())) {
                competitionsView.setNoMoreData();
            }
            competitionsView.showData(competitions);
        }, 1000);
    }

    /**
     * 根据登录状态判断刷新时该带条件查询还是该执行默认查询.
     */
    @Override
    public void getData(Integer page) {
        if (!getFiltering().equals(CompetitionTypeEnum.ALL)) {
            getReleasedCompetitionsByType(page, 10, getFiltering().getTypeCode());
        } else {
            Logger.d("调用无条件获取竞赛");
            getReleasedCompetitions(page, 10);
        }
    }
}
