package com.liukaixin.product.ru.business.user.myteams;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.team.teamdesc.TeamDescActivity;
import com.liukaixin.product.ru.data.model.Team;
import com.liukaixin.product.ru.data.model.UserTeam;
import com.orhanobut.logger.Logger;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Jacob on 2016/9/15.
 */
public class MyTeamsFragment extends Fragment implements MyTeamsContract.View,
        RecyclerArrayAdapter.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    private MyTeamsContract.Presenter presenter;

    private MyTeamsAdapter myTeamsAdapter;

    private EasyRecyclerView recyclerView;

    private Integer page = 1;

    public MyTeamsFragment() {

    }

    public static MyTeamsFragment newInStance() {
        return new MyTeamsFragment();
    }

    @Override
    public void setPresenter(MyTeamsContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_teams, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myTeamsAdapter = new MyTeamsAdapter(getActivity());

        // Set up my teams view
        recyclerView =
                (EasyRecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapterWithProgress(myTeamsAdapter);
        recyclerView.setRefreshListener(this);


        myTeamsAdapter.setMore(R.layout.view_more, this);
        myTeamsAdapter.setNoMore(R.layout.view_no_more);
        myTeamsAdapter.setOnItemClickListener(
                this::toTeamDescActivity);

        myTeamsAdapter.setError(R.layout.view_error).
                setOnClickListener(v -> myTeamsAdapter.resumeMore());

    }

    @Override
    public void onResume() {
        super.onResume();
        onRefresh();
    }

    private void toTeamDescActivity(Integer position) {
        Intent toTeamDesc = new Intent(getContext(), TeamDescActivity.class);
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_TEAM_ID,
                myTeamsAdapter.getItem(position).getTeamId());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_TEAM_NAME,
                myTeamsAdapter.getItem(position).getTeamName());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_CREATOR_ID,
                myTeamsAdapter.getItem(position).getCreatorId());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_COMPETITION_NAME,
                myTeamsAdapter.getItem(position).getCompetitionName());
        startActivity(toTeamDesc);
    }

    @Override
    public void onRefresh() {
        presenter.refresh();
    }

    @Override
    public void onLoadMore() {
        page++;
        presenter.loadMore(page);
    }

    @Override
    public void showError(String msg) {
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showData(List<UserTeam> teams) {
        recyclerView.setVisibility(View.VISIBLE);
        myTeamsAdapter.addAll(teams);
    }

    @Override
    public void showNoData() {
        recyclerView.showEmpty();
        myTeamsAdapter.clear();
    }

    @Override
    public void clearAdapter() {
        myTeamsAdapter.clear();
    }

    @Override
    public void initPage() {
        page = 1;
    }

    @Override
    public void setNoMoreData() {
        myTeamsAdapter.stopMore();
    }
}













