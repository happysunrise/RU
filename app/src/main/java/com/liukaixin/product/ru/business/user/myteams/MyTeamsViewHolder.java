package com.liukaixin.product.ru.business.user.myteams;

import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.UserTeam;

/**
 * Created by Jacob on 2016/9/16.
 */
public class MyTeamsViewHolder extends BaseViewHolder<UserTeam> {

    private TextView teamNameText;
    private TextView competitionNameText;

    public MyTeamsViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_my_team);
        teamNameText = $(R.id.name_text);
        competitionNameText = $(R.id.competition_name_text);
    }

    @Override
    public void setData(UserTeam team) {
        teamNameText.setText(team.getTeamName());
        competitionNameText.setText(team.getCompetitionName());
    }
}
