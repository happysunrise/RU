package com.liukaixin.product.ru.business.team.jointeamapply;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.BusinessException;
import com.liukaixin.product.ru.util.CheckUtil;
import com.orhanobut.logger.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/19.
 */
public class JoinTeamApplyInfoFragment extends Fragment
        implements JoinTeamApplyContract.View {

    private JoinTeamApplyContract.Presenter presenter;

    private TextInputLayout realNameTil;
    private TextInputLayout degreeTil;
    private TextInputLayout majorTil;
    private TextInputLayout contactTil;

    private EditText realNameEdit;
    private EditText degreeEdit;
    private EditText majorEdit;
    private EditText contactEdit;

    public JoinTeamApplyInfoFragment() {
        // Requires empty public constructor
    }

    public static JoinTeamApplyInfoFragment newInstance() {
        return new JoinTeamApplyInfoFragment();
    }

    @Override
    public void setPresenter(@NonNull JoinTeamApplyContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_join_team_apply_info,
                container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initViews(view);

        Button nextBtn = (Button) view.findViewById(R.id.next_btn);
        nextBtn.setOnClickListener(
                v -> {
                    if (!isEmpty(realNameEdit, realNameTil) &&
                            !isEmpty(degreeEdit, degreeTil) &&
                            !isEmpty(majorEdit, majorTil) &&
                            !isEmpty(contactEdit, contactTil)) {
                        Toast.makeText(getActivity(),
                                "clicked", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initViews(View view) {
        realNameTil = (TextInputLayout) view.findViewById(R.id.real_name_til);
        realNameEdit = (EditText) view.findViewById(R.id.real_name_edit);
        degreeTil = (TextInputLayout) view.findViewById(R.id.degree_til);
        degreeEdit = (EditText) view.findViewById(R.id.degree_edit);
        majorTil = (TextInputLayout) view.findViewById(R.id.major_til);
        majorEdit = (EditText) view.findViewById(R.id.major_edit);
        contactTil = (TextInputLayout) view.findViewById(R.id.contact_til);
        contactEdit = (EditText) view.findViewById(R.id.contact_edit);
    }

    @Override
    public void showLoading() {
        Toast.makeText(getActivity(), "loading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoading() {
        Toast.makeText(getActivity(), "hide loading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toSuccessActivity(String s) {
        Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    private boolean isEmpty(
            EditText editText,
            TextInputLayout textInputLayout) {
        try {
            CheckUtil.checkEmptyInput("此项",
                    editText.getText().toString().trim());
            editText.setError(null);
            textInputLayout.setError(null);
            return false;
        } catch (BusinessException e) {
            Logger.d("输入异常:", e);
            editText.setError(e.getMessage());
            textInputLayout.setError(e.getMessage());
            return true;
        }
    }
}
