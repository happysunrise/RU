package com.liukaixin.product.ru.enumerate;

/**
 * Created by liukaixin on 16/9/23.
 */

public enum TeamRoleTypeEnum {

    CREATOR(1, "创建者"),
    JOINER(0, "加入者");

    private final Integer typeCode;
    private final String description;

    TeamRoleTypeEnum(Integer typeCode, String description) {
        this.typeCode = typeCode;
        this.description = description;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public String getDescription() {
        return description;
    }

}
