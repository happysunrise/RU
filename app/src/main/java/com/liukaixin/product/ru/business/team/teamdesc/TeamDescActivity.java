package com.liukaixin.product.ru.business.team.teamdesc;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.liukaixin.product.ru.AppController;
import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.competition.competitiondesc.CompetitionDescActivity;
import com.liukaixin.product.ru.business.user.login.LoginActivity;
import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.data.model.Result;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.repositories.UserRepository;
import com.liukaixin.product.ru.enumerate.FavoriteTypeEnum;
import com.liukaixin.product.ru.enumerate.ResultTypeEnum;
import com.liukaixin.product.ru.util.ActivityUtils;
import com.liukaixin.product.ru.util.CheckUtil;

import static java.security.AccessController.getContext;

/**
 * Created by Jacob on 2016/9/19.
 */
public class TeamDescActivity extends AppCompatActivity {

    public static final String EXTRA_TEAM_ID = "TEAM_ID";
    public static final String EXTRA_TEAM_NAME = "TEAM_NAME";
    public static final String EXTRA_CREATOR_ID = "CREATOR_ID";
    public static final String EXTRA_COMPETITION_NAME = "COMPETITION_NAME";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_desc);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        TeamDescFragment teamDescFragment = (TeamDescFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_frame);

        Integer teamId = getIntent().getIntExtra(EXTRA_TEAM_ID, 1);
        String teamName = getIntent().getStringExtra(EXTRA_TEAM_NAME);
        Integer creatorId = getIntent().getIntExtra(EXTRA_CREATOR_ID, 1);
        String competitionName = getIntent().getStringExtra(EXTRA_COMPETITION_NAME);

        CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setTitle(teamName);


        if (teamDescFragment == null) {
            teamDescFragment = teamDescFragment.newInstance(teamId, creatorId,
                    teamName, competitionName);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    teamDescFragment, R.id.content_frame);
        }

        // Create the presenter
        new TeamDescPresenter(teamId, Injection.provideTeamsRepository(),
                teamDescFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
