package com.liukaixin.product.ru.business.team.jointeamapply;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liukaixin on 16/9/19.
 */

public class JoinTeamApplyActivity extends AppCompatActivity{
    public static final String EXTRA_TEAM_ID = "TEAM_ID";
    public static final String EXTRA_CREATOR_NAME = "CREATOR_ID";
    public static final String EXTRA_TEAM_NAME = "CREATOR_NAME";
    public static final String EXTRA_COMPETITION_NAME = "COMPETITION_NAME";
    // view pager
    private ViewPager viewPager;
    // 填充到ViewPager中的Fragment
    private List<Fragment> fragments;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_team_apply);
        // Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        Integer teamId = getIntent().getIntExtra(EXTRA_TEAM_ID, 1);
        Integer creatorId = getIntent().getIntExtra(EXTRA_CREATOR_NAME, 1);
        String teamName = getIntent().getStringExtra(EXTRA_TEAM_NAME);
        String competitionName = getIntent().getStringExtra(EXTRA_COMPETITION_NAME);
        // 初始化各种控件
        initViews();

        // 初始化mTitles、mFragments等ViewPager需要的数据
        initData(teamId, creatorId, teamName, competitionName);

        // 对各种控件进行设置、适配、填充数据
        configViews();
    }

    private void initViews() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
    }

    private void initData(Integer teamId, Integer creatorId,
                          String teamName, String competitionName) {
        fragments = new ArrayList<>();
//        JoinTeamApplyInfoFragment joinTeamApplyInfoFragment =
//                JoinTeamApplyInfoFragment.newInstance();
//        fragments.add(joinTeamApplyInfoFragment);
//        // Create the presenter
//        new JoinTeamApplyInfoPresenter(Injection.provideCompetitionsRepository(),
//                joinTeamApplyInfoFragment);

        JoinTeamApplyAbilityFragment joinTeamApplyAbilityFragment =
                JoinTeamApplyAbilityFragment.newInstance(teamId, creatorId,
                        teamName, competitionName);
        fragments.add(joinTeamApplyAbilityFragment);
        // Create the presenter
        new JoinTeamApplyAbilityPresenter(teamId,
                Injection.provideTeamsRepository(),
                joinTeamApplyAbilityFragment);
    }

    private void configViews() {
        // 初始化ViewPager的适配器，并设置给它
        JoinTeamViewPagerAdapter viewPagerAdapter =
                new JoinTeamViewPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(viewPagerAdapter);
        // 设置ViewPager最大缓存的页面个数
        viewPager.setOffscreenPageLimit(1);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
