package com.liukaixin.product.ru.service;

import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.data.model.Result;
import com.liukaixin.product.ru.data.model.Team;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.model.UserPush;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Completable;
import rx.Observable;

/**
 * 与用户相关的服务.
 *
 * Created by liukaixin on 16/9/3.
 */
public interface UserService {

    @POST("user/login")
    Observable<Result<User>> login(@Body User user);

    @POST("user/register")
    Observable<Result<User>> register(@Body User user);

    @GET("user/getUserInfoById/{userId}")
    Observable<Result<User>> getUserInfoById(@Path("userId") Integer userId);

    @POST("user/update")
    Observable<Result<String>> updateUser(@Body User user);

    @POST("user/favorite")
    Observable<Result<String>> favorite(@Body Favorite favorite);

    /**
     * 根据收藏id和用户id判断是否已收藏.
     *
     * @param starId 收藏id
     * @param userId 用户id
     * @return
     */
    @GET("user/getFavoriteOrNot/{starId}/{userId}")
    Observable<Result<Boolean>>
    getFavoriteOrNotByCompIdAndUserId(@Path("starId") Integer starId,
                                      @Path("userId") Integer userId);

    @DELETE("user/cancelFavorite/{starId}/{userId}")
    Observable<Result<String>>
    cancelFavorite(@Path("starId") Integer starId,
                   @Path("userId") Integer userId);

    @POST("user/push/addUserPush")
    Observable<Result<Integer>>
    addClientId(@Body UserPush userPush);
}
