package com.liukaixin.product.ru.business.team.teammember;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Apply;
import com.liukaixin.product.ru.data.model.User;

/**
 * Created by liukaixin on 16/9/19.
 */

public interface TeamMemberContract {

    interface View extends BaseView<Presenter> {

        void showData(User user);

        void showLoading();

        void hideLoading();

        void showError(String msg);
    }

    interface Presenter extends BasePresenter {
        void getMemberInfoByTeamIdAndUserId(Integer teamId, Integer userId);
    }

}
