package com.liukaixin.product.ru.business.user.myteams;

import android.content.Context;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Team;
import com.liukaixin.product.ru.data.model.UserTeam;

import java.util.List;

/**
 * Created by Jacob on 2016/9/15.
 */
public interface MyTeamsContract {
    interface View extends BaseView<Presenter>{

        void initPage();

        void showError(String msg);

        void showNoData();

        void clearAdapter();

        void setNoMoreData();

        void showData(List<UserTeam> teams);

        Context getContext();
    }

    interface Presenter extends BasePresenter{

        void refresh();

        void loadMore(Integer page);
    }
}
