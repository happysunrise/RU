package com.liukaixin.product.ru.business.team.jointeamapply;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Apply;


/**
 * Created by liukaixin on 16/9/19.
 */

public interface JoinTeamApplyContract {
    interface View extends BaseView<Presenter> {

        void showLoading();

        void hideLoading();

        void showError(String msg);

        void toSuccessActivity(String s);
    }

    interface Presenter extends BasePresenter {
        void sendAbility(Apply apply);
    }
}
