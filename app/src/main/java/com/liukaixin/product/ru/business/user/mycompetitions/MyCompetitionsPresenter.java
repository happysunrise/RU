package com.liukaixin.product.ru.business.user.mycompetitions;

import android.os.Handler;
import android.support.annotation.NonNull;

import com.liukaixin.product.ru.AppController;
import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Competition;
import com.liukaixin.product.ru.data.repositories.CompetitionsRepository;
import com.liukaixin.product.ru.util.ACache;
import com.orhanobut.logger.Logger;

import java.util.List;

/**
 * Created by Jacob on 2016/9/14.
 */
public class MyCompetitionsPresenter implements MyCompetitionsContract.Presenter {

    @NonNull
    private final CompetitionsRepository competitionsRepository;

    @NonNull
    private final MyCompetitionsContract.View myCompetitionsView;

    private Handler handler = new Handler();

    public MyCompetitionsPresenter(@NonNull CompetitionsRepository competitionsRepository,
                                   @NonNull MyCompetitionsContract.View myCompetitionsView) {

        this.competitionsRepository = competitionsRepository;
        this.myCompetitionsView = myCompetitionsView;

        this.myCompetitionsView.setPresenter(this);
    }

    public void refresh() {
        handler.postDelayed(() -> {
            myCompetitionsView.initPage();
            // 获取第一页的十条数据
            // TODO: 2016/9/15 delete after test
            getCompetitionByUserId(1, 10, 12);
        }, 1000);
    }

    public void loadMore(Integer page) {
        // TODO: 2016/9/15 delete after test
        getCompetitionByUserId(page, 10, 12);
    }

    private void getCompetitionByUserId(final Integer page,
                                        final Integer rows,
                                        final Integer userId) {
        Logger.d("userId = %d", userId);
        competitionsRepository.getCompetitionsByUserId(page, rows, userId,
                new CallBackListener<List<Competition>>() {

                    @Override
                    public void onCompleted() {
                        // do nothing
                    }

                    @Override
                    public void onError(String msg) {
                        myCompetitionsView.showError(msg);
                    }

                    @Override
                    public void onNext(List<Competition> myCompetitions) {
                        showDataOnView(myCompetitions, page);
                    }
                });
    }

    private void showDataOnView(final List<Competition> myCompetitions,
                                final Integer page) {
        handler.postDelayed(() -> {
            if (page == 1 && myCompetitions.isEmpty()) {
                myCompetitionsView.showNoData();
                return;
            }
            if (page == 1) {
                myCompetitionsView.clearAdapter();
            }
            if (myCompetitions.size() % 10 > 0
                    || (page != 1 && myCompetitions.isEmpty())) {
                myCompetitionsView.setNoMoreData();
            }
            myCompetitionsView.showData(myCompetitions);
        }, 1000);
    }
}
