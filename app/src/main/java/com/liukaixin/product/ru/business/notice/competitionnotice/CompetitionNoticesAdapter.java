package com.liukaixin.product.ru.business.notice.competitionnotice;

import android.content.Context;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.data.model.Notice;

/**
 * Created by Jacob on 2016/9/20.
 */
public class CompetitionNoticesAdapter extends RecyclerArrayAdapter<Notice> {

    public CompetitionNoticesAdapter(Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new CompetitionNoticesViewHolder(parent);
    }
}
