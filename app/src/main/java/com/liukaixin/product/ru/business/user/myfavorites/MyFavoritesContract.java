package com.liukaixin.product.ru.business.user.myfavorites;

import android.content.Context;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Favorite;

import java.util.List;

/**
 * Created by Jacob on 2016/9/16.
 */
public interface MyFavoritesContract {
    interface View extends BaseView<Presenter>{

        void showError(String msg);

        void showData(List<Favorite> favorites);

        void showNoData();

        void clearAdapter();

        void initPage();

        void setNoMoreData();

        Context getContext();
    }
    interface Presenter extends BasePresenter{

        void refresh();

        void loadMore(Integer page);
    }
}
