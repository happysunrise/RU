package com.liukaixin.product.ru.business.competition.competitiondesc;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Competition;
import com.liukaixin.product.ru.data.model.Favorite;

/**
 * Created by liukaixin on 16/9/18.
 */

public interface CompetitionDescContract {

    interface View extends BaseView<Presenter> {


        void showError(String msg);

        void showData(Competition competition);

        void showLoading();

        void hideLoading();

        void makeScrollViewVisible();

        void makeFavoriteFabClickable();

        void makeFavorited();

        void makeUnFavorited();

        void makeFavoriteFabUnClickable();
    }

    interface Presenter extends BasePresenter {

        void getCompetitionDesc();

        void getFavoriteStatus(Integer userId);

        void cancelFavorite(Integer userId);

        void doFavorite(Favorite favorite);
    }


}
