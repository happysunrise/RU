package com.liukaixin.product.ru.enumerate;

/**
 * Created by liukaixin on 16/9/14.
 */

public enum CompetitionTypeEnum {

    ELSE("14", "其他"),
    ART("13", "艺术设计"),
    IT_SCIENCE("12", "IT科技"),
    STARTUPS("11", "创业公益"),
    PROJECT("10", "学科学术"),
    ALL("15", "全部");

    private final String typeCode;
    private final String description;

    CompetitionTypeEnum(String typeCode, String description) {
        this.typeCode = typeCode;
        this.description = description;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public String getDescription() {
        return description;
    }
}
