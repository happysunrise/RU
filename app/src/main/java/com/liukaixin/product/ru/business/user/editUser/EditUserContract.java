package com.liukaixin.product.ru.business.user.editUser;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.User;

/**
 * Created by liukaixin on 16/9/20.
 */

public interface EditUserContract {
    interface View extends BaseView<Presenter> {

        void showResult(String result);

        void showError(String msg);

        void hideLoading();

        void showLoading();
    }

    interface Presenter extends BasePresenter {

        void updateUser(User user);
    }
}
