package com.liukaixin.product.ru.business.user.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.user.register.RegisterActivity;
import com.liukaixin.product.ru.util.LVCircularSmile;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/3.
 */

public class LoginFragment extends Fragment
        implements LoginContract.View, View.OnClickListener {

    private LoginContract.Presenter presenter;

    private EditText userNameEdit;

    private EditText passwordEdit;

    private LVCircularSmile smileLoading;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public LoginFragment() {
        // Required empty public constructor.
    }

    @Override
    public void setPresenter(@NonNull LoginContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userNameEdit = (EditText) view.findViewById(R.id.user_name_edit);
        passwordEdit = (EditText) view.findViewById(R.id.password_edit);

        Button okButton = (Button) view.findViewById(R.id.ok_button);
        okButton.setOnClickListener(this);

        Button registerButton = (Button) view.findViewById(R.id.register_button);
        registerButton.setOnClickListener(this);

        smileLoading = (LVCircularSmile) view.findViewById(R.id.lv_circularSmile);
        smileLoading.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.ok_button) {
            presenter.login();
        } else if (i == R.id.register_button) {
            Intent toRegister = new Intent(getActivity(), RegisterActivity.class);
            startActivity(toRegister);
            getActivity().finish();
        }
    }

    @Override
    public String getUsername() {
        return userNameEdit.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return passwordEdit.getText().toString().trim();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toLoginSuccessActivity() {
        Toast.makeText(getActivity(), "登录成功", Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override
    public void showUsernameEmpty() {
        userNameEdit.setError("用户名不能为空");
    }

    @Override
    public void showPasswordEmpty() {
        passwordEdit.setError("密码不能为空");
    }

    @Override
    public void showInvalidMail() {
        userNameEdit.setError("请输入正确的邮箱");
    }

    @Override
    public void showLoading() {
        smileLoading.setVisibility(View.VISIBLE);
        smileLoading.startAnim();
    }

    @Override
    public void hideLoading() {
        smileLoading.setVisibility(View.GONE);
        smileLoading.stopAnim();
    }

    @Override
    public void showPasswordFormatDismatch() {
        passwordEdit.setError("密码只能是8-16位数字和字母的组合");
    }

}
