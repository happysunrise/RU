package com.liukaixin.product.ru.service;

import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.data.model.Result;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Jacob on 2016/9/16.
 */
public interface FavoriteService {
    @GET("user/getFavoritesByUserId/{page}/{rows}/{userId}")
    Observable<Result<List<Favorite>>> getFavorites(
                    @Path("page") Integer page,
                    @Path("rows") Integer rows,
                    @Path("userId") Integer userId);
}
