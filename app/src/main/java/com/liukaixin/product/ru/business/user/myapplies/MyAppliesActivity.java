package com.liukaixin.product.ru.business.user.myapplies;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * Created by liukaixin on 16/10/5.
 */

public class MyAppliesActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_applies);

        // Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        MyAppliesFragment myAppliesFragment = (MyAppliesFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (myAppliesFragment == null) {
            myAppliesFragment = MyAppliesFragment.newInStance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    myAppliesFragment,R.id.content_frame);
        }

        // Create the presenter
        new MyAppliesPresenter(Injection.provideTeamsRepository(),
                myAppliesFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
