package com.liukaixin.product.ru.enumerate;

/**
 * Created by liukaixin on 16/9/3.
 */

public enum  BusinessExceptionEnum {

    EMPTY_USERNAME("1", "用户名为空"),
    EMPTY_PASSWORD("2", "密码为空"),
    INVALID_EMAIL("3", "邮箱不合法"),
    EMPTY_VERIFICATION("4", "验证码为空"),
    WRONG_VERIFICATION("5", "验证码错误"),
    CANNOT_BE_EMPTY("6", "不能为空");

    private final String value;
    private final String description;

    BusinessExceptionEnum(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
