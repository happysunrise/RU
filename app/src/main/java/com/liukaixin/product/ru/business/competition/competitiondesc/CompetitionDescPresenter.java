package com.liukaixin.product.ru.business.competition.competitiondesc;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Competition;
import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.data.model.Result;
import com.liukaixin.product.ru.data.repositories.CompetitionsRepository;
import com.liukaixin.product.ru.data.repositories.UserRepository;
import com.orhanobut.logger.Logger;

/**
 * Created by liukaixin on 16/9/18.
 */

public class CompetitionDescPresenter implements CompetitionDescContract.Presenter {
    @NonNull
    private final CompetitionsRepository competitionRepository;

    @NonNull
    private final CompetitionDescContract.View competitionDescView;

    @Nullable
    private Integer competitionId;

    private Handler handler = new Handler();

    public CompetitionDescPresenter(
            Integer competitionId,
            CompetitionsRepository competitionRepository,
            CompetitionDescContract.View competitionDescView) {
        this.competitionId = competitionId;
        this.competitionRepository = competitionRepository;
        this.competitionDescView = competitionDescView;

        this.competitionDescView.setPresenter(this);
    }

    /**
     * 获取竞赛详情.
     * <p>
     * 流程:
     * (1)显示加载层
     * (2)加载完成后隐藏加载层
     * (3)出错后显示出错层
     * (4)加载成功显示数据层.
     */
    @Override
    public void getCompetitionDesc() {
        competitionDescView.showLoading();
        competitionRepository.getCompetitionDescById(competitionId,
                new CallBackListener<Competition>() {
                    @Override
                    public void onCompleted() {
                        // do nothing
                    }

                    @Override
                    public void onError(String msg) {
                        competitionDescView.hideLoading();
                        competitionDescView.showError("获取竞赛出错T.T");
                    }

                    @Override
                    public void onNext(Competition competition) {
                        competitionDescView.hideLoading();
                        competitionDescView.makeScrollViewVisible();
                        competitionDescView.showData(competition);
                    }
                });
    }

    /**
     * 获取收藏状态.
     * <p>
     * 流程:
     * (1)获取完成后让收藏按钮变为可点击状态.
     * (2)获取错误后什么也不做.
     * (3)获取成功,如果收藏,则显示收藏状态,否则,则显示为收藏状态.
     *
     * @param userId 用户id
     */
    @Override
    public void getFavoriteStatus(Integer userId) {
        UserRepository userRepository = Injection.provideUserRepository();
        userRepository.getFavoriteStatusByStarIdAndUserId(
                competitionId, userId, new CallBackListener<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(String msg) {
                        // do nothing
                    }

                    @Override
                    public void onNext(Boolean result) {
                        competitionDescView.makeFavoriteFabClickable();
                        if (result) {
                            competitionDescView.makeFavorited();
                        } else {
                            competitionDescView.makeUnFavorited();
                        }
                    }
                });
    }

    /**
     * 收藏比赛.
     * <p>
     * 流程:
     * (1)显示已收藏,收藏按钮不可点击.
     * (2)加载完成后,收藏按钮可点击.
     * (3)加载错误后,什么也不做.
     * (4)加载成功,什么也不做.
     *
     * @param favorite 收藏
     */
    @Override
    public void doFavorite(Favorite favorite) {
        competitionDescView.showLoading();
        competitionDescView.makeFavoriteFabUnClickable();
        UserRepository userRepository = Injection.provideUserRepository();

        userRepository.favorite(favorite, new CallBackListener<Result<String>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(String msg) {
                competitionDescView.makeFavoriteFabClickable();
                competitionDescView.hideLoading();
            }

            @Override
            public void onNext(Result<String> result) {
                competitionDescView.makeFavorited();
                competitionDescView.makeFavoriteFabClickable();
                competitionDescView.hideLoading();
            }
        });
    }

    /**
     * 取消收藏比赛.
     * <p>
     * 流程:
     * (1)显示已取消收藏按钮不可点击.
     * (2)加载完成后,收藏按钮可点击.
     * (3)加载错误后,什么也不做.
     * (4)加载成功,什么也不做.
     *
     * @param userId 用户id
     */
    @Override
    public void cancelFavorite(Integer userId) {
        competitionDescView.showLoading();
        competitionDescView.makeFavoriteFabUnClickable();
        UserRepository userRepository = Injection.provideUserRepository();
        userRepository.cancelFavorite(competitionId, userId,
                new CallBackListener<Result<String>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(String msg) {
                        competitionDescView.makeFavoriteFabClickable();
                        competitionDescView.hideLoading();
                    }

                    @Override
                    public void onNext(Result<String> result) {
                        competitionDescView.makeUnFavorited();
                        competitionDescView.makeFavoriteFabClickable();
                        competitionDescView.hideLoading();
                    }
                });
    }
}
