package com.liukaixin.product.ru.service;

import com.liukaixin.product.ru.data.model.Notice;
import com.liukaixin.product.ru.data.model.Result;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Completable;
import rx.Observable;

/**
 * Created by Jacob on 2016/9/20.
 */
public interface NoticeService {
    @GET("notice/getNoticesByUserId/{page}/{rows}/{categoryId}/{userId}")
    Observable<Result<List<Notice>>> getNoticesByUserId(
            @Path("page") Integer page,
            @Path("rows") Integer rows,
            @Path("categoryId") Integer categoryId,
            @Path("userId") Integer userId);
}
