package com.liukaixin.product.ru.business.team.teamlist;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Team;

/**
 * TeamsViewHolder.
 *
 * Created by Jacob on 2016/9/5.
 */
public class TeamsViewHolder extends BaseViewHolder<Team> {

    private ImageView picImg;
    private TextView nameText;
    private TextView teamNeedText;
    private TextView leaderSchoolText;
    private TextView competitionNameText;
    private TextView teamMemberNumText;

    public TeamsViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_team);
        picImg = $(R.id.image_view);
        nameText = $(R.id.name_text);
        teamNeedText = $(R.id.team_need_text);
        leaderSchoolText = $(R.id.leader_school_text);
        competitionNameText = $(R.id.competition_name_text);
        teamMemberNumText = $(R.id.team_member_num_text);
    }

    public void setData(final Team team) {
        Glide.with(getContext())
                .load(team.getPic())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(picImg);
        nameText.setText(team.getName());
        teamNeedText.setText(team.getRequire());
        leaderSchoolText.setText("");
        competitionNameText.setText(team.getCompetitionName());
        teamMemberNumText.setText("差" + String.valueOf(team.getNumber()) + "人");
    }
}
