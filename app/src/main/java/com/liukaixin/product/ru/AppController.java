package com.liukaixin.product.ru;

import android.app.Application;

import com.liukaixin.product.ru.util.ACache;

/**
 * Created by liukaixin on 16/9/1.
 */

public class AppController extends Application {

    private static AppController INSTANCE;

    private ACache aCache;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        this.aCache = ACache.get(this);
    }

    public static synchronized AppController getInstance() {
        return INSTANCE;
    }

    public ACache getCache() {
        return this.aCache;
    }
}
