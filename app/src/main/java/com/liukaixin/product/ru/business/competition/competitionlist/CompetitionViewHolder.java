package com.liukaixin.product.ru.business.competition.competitionlist;

import android.text.Html;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Competition;
import com.liukaixin.product.ru.manager.CompetitionManager;

/**
 * Created by liukaixin on 16/8/30.
 */
public class CompetitionViewHolder extends BaseViewHolder<Competition> {

    private TextView nameText;
    private TextView typeText;
    private TextView signUpEndTimeText;
    private ImageView picImg;
    private TextView introText;


    public CompetitionViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_competition);
        nameText = $(R.id.name_text);
        typeText = $(R.id.type_text);
        signUpEndTimeText = $(R.id.sign_up_end_time_text);
        picImg = $(R.id.pic_img);
        introText = $(R.id.intro_text);
    }

    @Override
    public void setData(final Competition competition){
        nameText.setText(competition.getName());
        typeText.setText(CompetitionManager.handleTypesIds(competition.getTypeIds()));
        signUpEndTimeText.setText("报名截止：" + competition.getSignUpEndTime().split(" ")[0]);
        introText.setText(Html.fromHtml(competition.getIntro()));
        Glide.with(getContext())
                .load(competition.getPic())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(picImg);
    }
}
