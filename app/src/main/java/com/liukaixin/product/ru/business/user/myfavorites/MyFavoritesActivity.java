package com.liukaixin.product.ru.business.user.myfavorites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * Created by Jacob on 2016/9/16.
 */
public class MyFavoritesActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favorites);

        // Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        MyFavoritesFragment myFavoritesFragment = (MyFavoritesFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if(myFavoritesFragment == null) {
            myFavoritesFragment = MyFavoritesFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    myFavoritesFragment, R.id.content_frame);
        }

        // Create the presenter
        new MyFavoritesPresenter(Injection.provideMyFavoritesRepository(),
                myFavoritesFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
