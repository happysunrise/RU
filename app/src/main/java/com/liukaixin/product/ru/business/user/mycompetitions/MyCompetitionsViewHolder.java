package com.liukaixin.product.ru.business.user.mycompetitions;

import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Competition;

/**
 * Created by Jacob on 2016/9/14.
 */
public class MyCompetitionsViewHolder extends BaseViewHolder<Competition>{

    private TextView nameText;
    private TextView signUpEndTimeText;

    public MyCompetitionsViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_my_competition);
        nameText = $(R.id.name_text);
        signUpEndTimeText = $(R.id.sign_up_end_time_text);
    }

    @Override
    public void setData(Competition competition) {
        nameText.setText(competition.getName());
        signUpEndTimeText.setText(competition.getSignUpEndTime());
    }
}
