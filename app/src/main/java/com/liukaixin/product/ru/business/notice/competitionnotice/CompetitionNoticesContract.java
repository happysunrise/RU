package com.liukaixin.product.ru.business.notice.competitionnotice;

import android.content.Context;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Notice;

import java.util.List;

/**
 * Created by Jacob on 2016/9/20.
 */
public interface CompetitionNoticesContract {

    interface View extends BaseView<Presenter> {

        void initPage();

        void showError(String msg);

        void showNoData();

        void clearAdapter();

        void setNoMoreData();
        
        void showData(List<Notice> notice);

        Context getContext();
    }

    interface Presenter extends BasePresenter {

        void loadMore(Integer page, Integer categoryId);

        void refresh(Integer categoryId);
    }

}
