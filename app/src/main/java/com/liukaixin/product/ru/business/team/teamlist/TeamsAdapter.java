package com.liukaixin.product.ru.business.team.teamlist;

import android.content.Context;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.data.model.Team;

/**
 * 团队列表适配器.
 *
 * Created by Jacob on 2016/9/5.
 */
public class TeamsAdapter extends RecyclerArrayAdapter<Team> {

    public TeamsAdapter(Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new TeamsViewHolder(parent);
    }
}
