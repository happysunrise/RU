package com.liukaixin.product.ru.business.user.editUser;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.liukaixin.product.ru.AppController;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.team.jointeamapply.JoinTeamApplyAbilityFragment;
import com.liukaixin.product.ru.business.team.jointeamapply.JoinTeamApplyActivity;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.enumerate.ResultTypeEnum;
import com.liukaixin.product.ru.util.BusinessException;
import com.liukaixin.product.ru.util.CheckUtil;
import com.liukaixin.product.ru.util.LVCircularSmile;
import com.orhanobut.logger.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/20.
 */

public class EditUserFragment extends Fragment implements EditUserContract.View {

    private EditUserContract.Presenter presenter;

    private LVCircularSmile loadingSmile;

    private TextInputLayout nickNameTil, schoolTil, majorTil;

    public EditUserFragment() {
        // Required empty public constructor
    }

    public static EditUserFragment newInstance() {
        return new EditUserFragment();
    }

    @Override
    public void setPresenter(@NonNull EditUserContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_user, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        User user = JSON.parseObject(AppController.getInstance().getCache().getAsString("user"),
                User.class);

        loadingSmile = (LVCircularSmile) view.findViewById(R.id.lv_circularSmile);
        nickNameTil = (TextInputLayout) view.findViewById(R.id.nick_name_til);
        schoolTil = (TextInputLayout) view.findViewById(R.id.school_til);
        majorTil = (TextInputLayout) view.findViewById(R.id.major_til);

        EditText nickNameEdit = (EditText) view.findViewById(R.id.nick_name_edit);
        if (user.getNickName() != null) {
            nickNameEdit.setText(user.getNickName());
        }
        EditText realNameEdit = (EditText) view.findViewById(R.id.real_name_edit);
        if (user.getRealName() != null) {
            realNameEdit.setText(user.getRealName());
        }
        RadioGroup genderRg = (RadioGroup) view.findViewById(R.id.gender_rg);
        if (user.getGender() != null) {
            if (user.getGender().equals("男")) {
                RadioButton boyRb = (RadioButton) view.findViewById(R.id.boy_rb);
                boyRb.setChecked(true);
            } else {
                RadioButton girRb = (RadioButton) view.findViewById(R.id.girl_rb);
                girRb.setChecked(true);
            }
        }
        EditText schoolEdit = (EditText) view.findViewById(R.id.school_edit);
        if (user.getSchool() != null) {
            schoolEdit.setText(user.getSchool());
        }
        EditText majorEdit = (EditText) view.findViewById(R.id.major_edit);
        if (user.getMajor() != null) {
            majorEdit.setText(user.getMajor());
        }
        RadioGroup degreeRg = (RadioGroup) view.findViewById(R.id.degree_rg);
        Button saveBtn = (Button) view.findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(v -> {
            if (nickNameEdit.getText().toString().trim().isEmpty()) {
                nickNameTil.setError("此项不能为空");
            } else if (schoolEdit.getText().toString().trim().isEmpty()) {
                schoolTil.setError("此项不能为空");
            } else if (majorEdit.getText().toString().trim().isEmpty()) {
                majorTil.setError("此项不能为空");
            } else {
                user.setRealName(
                        realNameEdit.getText().toString().trim());
                user.setNickName(
                        nickNameEdit.getText().toString().trim());
                if (genderRg.getCheckedRadioButtonId() == R.id.boy_rb) {
                    user.setGender("男");
                } else {
                    user.setGender("女");
                }
                user.setSchool(
                        schoolEdit.getText().toString().trim());
                user.setMajor(
                        majorEdit.getText().toString().trim());
                if (degreeRg.getCheckedRadioButtonId() == R.id.b_rb) {
                    user.setDegree("本科");
                } else if (degreeRg.getCheckedRadioButtonId() == R.id.z_rb) {
                    user.setDegree("专科");
                } else if (degreeRg.getCheckedRadioButtonId() == R.id.s_rb) {
                    user.setDegree("硕士");
                } else {
                    user.setDegree("博士");
                }
                presenter.updateUser(user);
            }
        });
    }

    @Override
    public void showResult(String result) {
        Toast.makeText(getActivity(), "修改" + result, Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoading() {
        loadingSmile.setVisibility(View.GONE);
        loadingSmile.stopAnim();
    }

    @Override
    public void showLoading() {
        loadingSmile.setVisibility(View.VISIBLE);
        loadingSmile.startAnim();
    }
}
