package com.liukaixin.product.ru.business.team.check;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Apply;
import com.liukaixin.product.ru.data.model.User;

/**
 * Created by Jacob on 2016/9/21.
 */
public interface CheckContract {

    interface View extends BaseView<Presenter> {
        void showError(String msg);

        void showData();

        void showLoading();

        void hideLoading();

        void setApply(Apply result);

        void showNotCheckView();

        void showAgreeView();

        void showRejectView();
    }
    interface Presenter extends BasePresenter{

        void checkApply(Integer status);

        void getApplyById();

        void getOtherApplyStatus(Integer applyUserId,
                                 String competitionName, Integer typeCode);
    }
}
