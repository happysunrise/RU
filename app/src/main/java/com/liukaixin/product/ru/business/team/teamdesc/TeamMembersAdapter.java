package com.liukaixin.product.ru.business.team.teamdesc;

import android.content.Context;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.business.competition.competitionlist.CompetitionViewHolder;
import com.liukaixin.product.ru.data.model.Competition;
import com.liukaixin.product.ru.data.model.TeamMember;

/**
 * Created by liukaixin on 16/8/29.
 */
public class TeamMembersAdapter extends
        RecyclerArrayAdapter<TeamMember> {

    public TeamMembersAdapter(Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new TeamMembersViewHolder(parent);
    }
}
