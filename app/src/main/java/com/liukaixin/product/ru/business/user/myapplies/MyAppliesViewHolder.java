package com.liukaixin.product.ru.business.user.myapplies;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Apply;
import com.liukaixin.product.ru.enumerate.ApplyStatusEnum;

/**
 * Created by liukaixin on 16/10/5.
 */

public class MyAppliesViewHolder extends BaseViewHolder<Apply> {

    private TextView teamNameText;
    private TextView competitionNameText;
    private ImageView statusImg;
    private TextView createTimeText;
    private TextView abilityText;

    public MyAppliesViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_my_apply);
        teamNameText = $(R.id.team_name_text);
        competitionNameText = $(R.id.competition_name_text);
        statusImg = $(R.id.status_img);
        createTimeText = $(R.id.create_time_text);
        abilityText = $(R.id.ability_text);
    }

    @Override
    public void setData(Apply apply) {
        if (apply.getStatus().equals(ApplyStatusEnum.NOT_CHECK.getTypeCode())) {
            statusImg.setImageResource(R.drawable.not_check);
        } else if (apply.getStatus().equals(ApplyStatusEnum.AGREE.getTypeCode())) {
            statusImg.setImageResource(R.drawable.little_agree);
        } else {
            statusImg.setImageResource(R.drawable.little_reject);
        }
        abilityText.setText(apply.getAbility());
        createTimeText.setText(apply.getCreateTime());
        teamNameText.setText(apply.getTeamName());
        competitionNameText.setText(apply.getCompetitionName());
    }

}
