package com.liukaixin.product.ru.service;

import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Apply;
import com.liukaixin.product.ru.data.model.Result;
import com.liukaixin.product.ru.data.model.Team;
import com.liukaixin.product.ru.data.model.TeamMember;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.model.UserTeam;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Completable;
import rx.Observable;

/**
 * Created by Jacob on 2016/9/5.
 */
public interface TeamsService {
    /**
     * 获取团队
     *
     * @param page 页码
     * @param rows 条数
     * @return 团队列表
     */
    @GET("team/getReleasedTeams/{page}/{rows}")
    Observable<Result<List<Team>>> getReleasedTeams(
            @Path("page") Integer page,
            @Path("rows") Integer rows);

    /**
     * 根据用户Id获取我的团队
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户Id
     * @return 返回用户团队列表
     */
    @GET("user/getTeamsByUserId/{page}/{rows}/{userId}")
    Observable<Result<List<UserTeam>>> getTeamsByUserId(
            @Path("page") Integer page,
            @Path("rows") Integer rows,
            @Path("userId") Integer userId);

    /**
     * 根据Id 获取团队
     *
     * @param id id
     * @return
     */
    @GET("team/getTeamDescById/{id}")
    Observable<Result<Team>> getTeamDescById(
            @Path("id") Integer id);

    /**
     * 提交加入团队申请.
     *
     * @param apply 加入申请
     * @return
     */
    @POST("team/apply/addApply")
    Observable<Result<String>> sendAbility(@Body Apply apply);

    /**
     * 根据团队id和用户id获取团队成员详情.
     *
     * @param teamId 团队id
     * @param userId 用户id
     * @return
     */
    @GET("team/getTeamMemberInfoByTeamIdAndUserId/{teamId}/{userId}")
    Observable<Result<User>> getTeamMemberInfoByTeamIdAndUserId(
            @Path("teamId") Integer teamId,
            @Path("userId") Integer userId);

    @POST("team/createTeam")
    Observable<Result<String>> sendCreatedTeam(@Body Team team);

    /**
     * 根据竞赛id获取团队列表.
     *
     * @param page          页码
     * @param rows          条数
     * @param competitionId 竞赛id
     * @return
     */
    @GET("team/getTeamsByCompetitionId/{page}/{rows}/{competitionId}")
    Observable<Result<List<Team>>>
    getTeamsByCompetitionId(@Path("page") Integer page,
                            @Path("rows") Integer rows,
                            @Path("competitionId") Integer competitionId);

    /**
     * 根据团队id和申请人id查询是否已申请过.
     *
     * @param teamId 团队id
     * @param userId 申请人id
     * @return
     */
    @GET("team/apply/getApplyStatus/{teamId}/{userId}")
    Observable<Result<Integer>>
    getApplyStatus(@Path("teamId") Integer teamId,
                   @Path("userId") Integer userId);


    /**
     * 根据申请id获取申请.
     *
     * @param applyId 申请id
     * @return
     */
    @GET("team/apply/getApplyById/{id}")
    Observable<Result<Apply>>
    getApplyById(@Path("id") Integer applyId);

    @PUT("team/apply/updateApplyStatus/{applyId}/{status}")
    Observable<Result<String>>
    updateApplyStatus(@Path("applyId") Integer applyId,
                      @Path("status") Integer status);

    /**
     * 添加我的团队.
     *
     * @param userTeam
     * @return
     */
    @POST("user/addMyTeam")
    Observable<Result<String>>
    addToMyTeam(@Body UserTeam userTeam);

    /**
     * 查找该团队申请加入者申请同比赛的其他团队的状态.
     *
     * @param applyUserId     申请人id
     * @param competitionName 比赛名称
     * @return 申请状态
     */
    @GET("team/apply/getOtherApplyStatus/{applyUserId}/{competitionName}")
    Observable<Result<Integer>>
    getOtherApplyStatus(@Path("applyUserId") Integer applyUserId,
                        @Path("competitionName") String competitionName);


    @GET("team/apply/getMyAppliesByUserId/{page}/{rows}/{userId}")
    Observable<Result<List<Apply>>>
    getMyAppliesByUserId(@Path("page") Integer page,
                         @Path("rows") Integer rows,
                         @Path("userId") Integer userId);

    @GET("team/getTeamMembers/{teamId}")
    Observable<Result<List<TeamMember>>>
    getTeamMembers(@Path("teamId") Integer teamId);
}
