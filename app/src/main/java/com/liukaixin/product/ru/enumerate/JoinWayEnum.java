package com.liukaixin.product.ru.enumerate;

/**
 * Created by liukaixin on 16/9/21.
 */

public enum JoinWayEnum {

    TEAM(1, "团队赛"),
    SINGLE(0, "个人赛");

    private final Integer typeCode;
    private final String description;

    JoinWayEnum(Integer typeCode, String description) {
        this.typeCode = typeCode;
        this.description = description;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public String getDescription() {
        return description;
    }
}
