package com.liukaixin.product.ru.business.team.createteam;

import android.content.Context;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Team;

/**
 * Created by Jacob on 2016/9/20.
 */
public interface CreateTeamContract {

    interface View extends BaseView<Presenter> {

        void showError(String msg);

        void showLoading();

        void hideLoading();

        void toSuccessCreatedTeam(String s);

        Context getContext();
    }

    interface Presenter extends BasePresenter {

        void sendCreatedTeam(Team team);
    }
}
