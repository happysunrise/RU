package com.liukaixin.product.ru.data.repositories;

import android.support.annotation.NonNull;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.liukaixin.product.ru.AppController;
import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.data.model.Result;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.service.FavoriteService;
import com.liukaixin.product.ru.service.ServiceFactory;
import com.liukaixin.product.ru.util.ACache;
import com.orhanobut.logger.Logger;

import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Jacob on 2016/9/16.
 */
public class MyFavoritesRepository {

    private static MyFavoritesRepository INSTANCE = null;

    private FavoriteService favoriteService;

    private ACache aCache = AppController.getInstance().getCache();

    private boolean isCacheDirty = false;

    private MyFavoritesRepository() {
        this.favoriteService = ServiceFactory
                .createRetrofitService(FavoriteService.class,
                        ServiceFactory.BASE_URL);
    }

    public static MyFavoritesRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MyFavoritesRepository();
        }
        return INSTANCE;
    }

    public void getFavoritesByUserId(final Integer page,
                                     final Integer rows,
                                     @NonNull final CallBackListener<List<Favorite>> callback) {
        checkNotNull(callback);

        User user = JSON.parseObject(AppController.getInstance().getCache().getAsString("user"),
                User.class);

        favoriteService.getFavorites(page, rows, user.getId())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Result<List<Favorite>>>() {
                    @Override
                    public void onCompleted() {
                        callback.onCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e, e.getMessage());
                        callback.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(Result<List<Favorite>> result) {
                        if (result.getCode().equals(Result.SUCCESS)) {
                            callback.onNext(result.getBean());
                        } else {
                            callback.onError(result.getMessage());
                        }
                    }
                });
    }

    public void refresh() {
        isCacheDirty = true;
    }

    private void putInCache(String key, String value) {
        aCache.put(key, value, ACache.TIME_HOUR);
        isCacheDirty = false;
    }
}
