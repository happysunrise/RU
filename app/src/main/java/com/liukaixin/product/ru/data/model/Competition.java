package com.liukaixin.product.ru.data.model;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 竞赛.
 * <p>
 * Created by liukaixin on 16/8/30.
 */

public class Competition {

    /**
     * 比赛id.
     */
    private Integer id;

    /**
     * 比赛名称.
     */
    private String name;

    /**
     * 比赛图片.
     */
    private String pic;

    /**
     * 举办方/举办机构.
     */
    private String host;

    /**
     * 举办方id.
     */
    private Integer hostUserId;

    /**
     * 比赛简介.
     */
    private String intro;

    /**
     * 地区名称
     */
    private String area;

    /**
     * 比赛主题.
     */
    private String theme;

    /**
     * 赛项.
     */
    private String item;

    /**
     * 赛程安排.
     */
    private String schedule;

    /**
     * 奖品.
     */
    private String award;

    /**
     * 参赛形式,0:个人 1：团队.
     */
    private Integer joinWay;

    /**
     * 参赛要求.
     */
    private String require;

    /**
     * 官网.
     */
    private String web;

    /**
     * 比赛类型id,格式为11,13,14…….
     */
    private String typeIds;

    /**
     * 报名截止日期.
     */
    @JSONField(format = "yyyy-MM-dd")
    private String signUpEndTime;

    /**
     * 预计归档日期,暂定为报名截止后10天.
     */
    @JSONField(format = "yyyy-MM-dd")
    private String predictAchieveTime;

    /**
     * 状态,0:禁止  1:审核   2:发布   3:归档.
     */
    private Integer status;

    @JSONField(format = "yyyy-MM-dd")
    private String createTime;

    @JSONField(format = "yyyy-MM-dd")
    private String updateTime;

    /**
     * 点赞数.
     */
    private Integer like;

    /**
     * 浏览量.
     */
    private Integer hit;

    /**
     * 参赛年级.
     */
    private String degree;

    public Competition() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getHostUserId() {
        return hostUserId;
    }

    public void setHostUserId(Integer hostUserId) {
        this.hostUserId = hostUserId;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getAward() {
        return award;
    }

    public void setAward(String award) {
        this.award = award;
    }

    public Integer getJoinWay() {
        return joinWay;
    }

    public void setJoinWay(Integer joinWay) {
        this.joinWay = joinWay;
    }

    public String getRequire() {
        return require;
    }

    public void setRequire(String require) {
        this.require = require;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(String typeIds) {
        this.typeIds = typeIds;
    }

    public String getSignUpEndTime() {
        return signUpEndTime;
    }

    public void setSignUpEndTime(String signUpEndTime) {
        this.signUpEndTime = signUpEndTime;
    }

    public String getPredictAchieveTime() {
        return predictAchieveTime;
    }

    public void setPredictAchieveTime(String predictAchieveTime) {
        this.predictAchieveTime = predictAchieveTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLike() {
        return like;
    }

    public void setLike(Integer like) {
        this.like = like;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    @Override
    public String toString() {
        return "Competition{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pic='" + pic + '\'' +
                ", host='" + host + '\'' +
                ", hostUserId=" + hostUserId +
                ", intro='" + intro + '\'' +
                ", area='" + area + '\'' +
                ", theme='" + theme + '\'' +
                ", item='" + item + '\'' +
                ", schedule='" + schedule + '\'' +
                ", award='" + award + '\'' +
                ", joinWay=" + joinWay +
                ", require='" + require + '\'' +
                ", web='" + web + '\'' +
                ", typeIds='" + typeIds + '\'' +
                ", signUpEndTime='" + signUpEndTime + '\'' +
                ", predictAchieveTime='" + predictAchieveTime + '\'' +
                ", status=" + status +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", like=" + like +
                ", hit=" + hit +
                ", degree='" + degree + '\'' +
                '}';
    }
}
