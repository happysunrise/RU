package com.liukaixin.product.ru.business.user.myapplies;

import android.content.Context;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.data.model.Apply;

/**
 * Created by liukaixin on 16/10/5.
 */

public class MyAppliesAdapter extends RecyclerArrayAdapter<Apply> {

    public MyAppliesAdapter(Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyAppliesViewHolder(parent);
    }

}
