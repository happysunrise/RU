package com.liukaixin.product.ru.business.user.myapplies;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Apply;
import com.orhanobut.logger.Logger;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/10/5.
 */

public class MyAppliesFragment extends Fragment implements MyAppliesContract.View,
        RecyclerArrayAdapter.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    private MyAppliesContract.Presenter presenter;

    private MyAppliesAdapter myAppliesAdapter;

    private EasyRecyclerView recyclerView;

    private Integer page = 1;

    public MyAppliesFragment() {

    }

    public static MyAppliesFragment newInStance() {
        return new MyAppliesFragment();
    }

    @Override
    public void setPresenter(MyAppliesContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_applies, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myAppliesAdapter = new MyAppliesAdapter(getActivity());
        // Set up my teams view
        recyclerView =
                (EasyRecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapterWithProgress(myAppliesAdapter);
        recyclerView.setRefreshListener(this);

        myAppliesAdapter.setMore(R.layout.view_more, this);
        myAppliesAdapter.setNoMore(R.layout.view_no_more);

        myAppliesAdapter.setError(R.layout.view_error).
                setOnClickListener(v -> myAppliesAdapter.resumeMore());

        onRefresh();
    }

    @Override
    public void onRefresh() {
        presenter.refresh();
    }

    @Override
    public void onLoadMore() {
        page++;
        presenter.loadMore(page);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(List<Apply> applies) {
        myAppliesAdapter.addAll(applies);
    }

    @Override
    public void showNoData() {
        recyclerView.showEmpty();
        myAppliesAdapter.clear();
    }

    @Override
    public void clearAdapter() {
        myAppliesAdapter.clear();
    }

    @Override
    public void initPage() {
        page = 1;
    }

    @Override
    public void setNoMoreData() {
        myAppliesAdapter.stopMore();
    }
}
