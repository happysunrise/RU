package com.liukaixin.product.ru.business.user.myfavorites;

import android.content.Context;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.data.model.Favorite;


/**
 * Created by Jacob on 2016/9/16.
 */
public class MyFavoritesAdapter extends RecyclerArrayAdapter<Favorite> {

    public MyFavoritesAdapter(Context context) {
        super(context);
    }


    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyFavoritesViewHolder(parent);
    }


}
