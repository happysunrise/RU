package com.liukaixin.product.ru.data.model;

/**
 * Created by Jacob on 2016/9/5.
 */
public class Team {

    /**
     *  团队ID.
     */
    private Integer id;
    /**
     *  团队名称.
     */
    private String name;

    /**
     *  团队照片.
     */
    private String pic;

    /**
     * 比赛名称.
     */
    private String competitionName;

    /**
     * 比赛id.
     */
    private Integer competitionId;

    /**
     * 团队创始人.
     */
    private Integer userId;

    /**
     * 团队创始人昵称.
     */
    private String userNickName;

    /**
     * 团队所属地区.
     */
    private String areaIds;

    /**
     * 团队创始人学校名称.
     */
    private String school;

    /**
     * 团队成员.
     */
    private String memberIds;

    /**
     * 团队人数
     */
    private Integer number;

    /**
     * 团队简介
     */
    private String intro;

    /**
     * 预计归档时间.
     */
    private String predictAchieveTime;

    /**
     * 团队状态.
     */
    private String status;

    /**
     * 团队创建时间.
     */
    private String createTime;

    /**
     * 团队更新时间.
     */
    private String updateTime;

    /**
     * 团队座右铭.
     */
    private String motto;

    /**
     * 团队需求.
     */
    private String require;

    /**
     * 创始人简介.
     */
    private String creatorIntro;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public String getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(String areaIds) {
        this.areaIds = areaIds;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(String memberIds) {
        this.memberIds = memberIds;
    }

    public String getPredictAchieveTime() {
        return predictAchieveTime;
    }

    public void setPredictAchieveTime(String predictAchieveTime) {
        this.predictAchieveTime = predictAchieveTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getIntro(){
        return intro;
    }

    public void setIntro(String intro){
        this.intro = intro;
    }

    public String  getPic(){
        return pic;
    }

    public void setPic(String pic){
        this.pic = pic;
    }

    public Integer getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Integer competitionId) {
        this.competitionId = competitionId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Team(String teamName,
                String teamIntro,
                String require,
                Integer teamNumber, String creatorIntro) {
        this.name = teamName;
        this.intro = teamIntro;
        this.require = require;
        this.number = teamNumber;
        this.creatorIntro = creatorIntro;
    }

    public String getRequire() {
        return require;
    }

    public void setRequire(String require) {
        this.require = require;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getCreatorIntro() {
        return creatorIntro;
    }

    public void setCreatorIntro(String creatorIntro) {
        this.creatorIntro = creatorIntro;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pic='" + pic + '\'' +
                ", competitionName='" + competitionName + '\'' +
                ", competitionId=" + competitionId +
                ", userId=" + userId +
                ", userNickName='" + userNickName + '\'' +
                ", areaIds='" + areaIds + '\'' +
                ", school='" + school + '\'' +
                ", memberIds='" + memberIds + '\'' +
                ", number=" + number +
                ", intro='" + intro + '\'' +
                ", predictAchieveTime='" + predictAchieveTime + '\'' +
                ", status='" + status + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", motto='" + motto + '\'' +
                ", require='" + require + '\'' +
                ", creatorIntro='" + creatorIntro + '\'' +
                '}';
    }
}
