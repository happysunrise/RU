package com.liukaixin.product.ru.business.competition.competitiondesc;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * Created by liukaixin on 16/9/18.
 */

public class CompetitionDescActivity extends AppCompatActivity {

    public static final String EXTRA_COMPETITION_ID = "COMPETITION_ID";
    public static final String EXTRA_PIC_URL = "http://img15.3lian.com/2015/f2/116/d/46.jpg";
    public static final String EXTRA_COMPETITION_NAME = "COMPETITION_NAME";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_competition_desc);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
//
//        CollapsingToolbarLayout collapsingToolbarLayout
//                = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);

        // Get the requested competition id
        Integer competitionId = getIntent().getIntExtra(EXTRA_COMPETITION_ID, 1);
        // Set the competition picture
        String picUrl = getIntent().getStringExtra(EXTRA_PIC_URL);
        // Set the competition name
        String competitionName = getIntent().getStringExtra(EXTRA_COMPETITION_NAME);

//        collapsingToolbarLayout.setTitle(competitionName);
//        collapsingToolbarLayout.setExpandedTitleColor(
//                getResources().getColor(R.color.liukx_primary_dark));
//        collapsingToolbarLayout.setCollapsedTitleTextColor(
//                getResources().getColor(R.color.liukx_text_primary));

        ImageView competitionImg = (ImageView) findViewById(R.id.competition_img);
        Glide.with(this)
                .load(picUrl)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(competitionImg);

        CompetitionDescFragment competitionDescFragment
                = (CompetitionDescFragment) getSupportFragmentManager()
                .findFragmentById(R.id.content_frame);

        if (competitionDescFragment == null) {
            competitionDescFragment = CompetitionDescFragment.newInstance(
                    competitionId, competitionName);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    competitionDescFragment, R.id.content_frame);
        }

        // Create the presenter
        new CompetitionDescPresenter(competitionId, Injection.provideCompetitionsRepository(),
                competitionDescFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
