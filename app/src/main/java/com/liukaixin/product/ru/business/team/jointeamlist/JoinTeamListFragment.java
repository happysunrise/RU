package com.liukaixin.product.ru.business.team.jointeamlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.team.teamdesc.TeamDescActivity;
import com.liukaixin.product.ru.data.model.Team;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Jacob on 2016/9/18.
 */
public class JoinTeamListFragment extends Fragment implements JoinTeamListContract.View, RecyclerArrayAdapter.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener {

    @NonNull
    private static final String ARGUMENT_COMPETITION_ID = "COMPETITION_ID";

    private JoinTeamListContract.Presenter presenter;

    private JoinTeamListAdapter joinTeamListAdapter;

    private EasyRecyclerView recyclerView;

    private Integer page = 1;

    public JoinTeamListFragment() {

    }

    public static JoinTeamListFragment newInstance(@Nullable Integer competitionId) {
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_COMPETITION_ID, competitionId);
        JoinTeamListFragment fragment = new JoinTeamListFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void setPresenter(@NonNull JoinTeamListContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_join_team_list, container, false);
        joinTeamListAdapter =
                new JoinTeamListAdapter(getActivity());
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set up my teams view
        recyclerView =
                (EasyRecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapterWithProgress(joinTeamListAdapter);
        recyclerView.setRefreshListener(this);

        joinTeamListAdapter.setMore(R.layout.view_more, this);
        joinTeamListAdapter.setNoMore(R.layout.view_no_more);
        joinTeamListAdapter.setOnItemClickListener(
                this::toTeamDescActivity);

        joinTeamListAdapter.setError(R.layout.view_error).
                setOnClickListener(v -> joinTeamListAdapter.resumeMore());

        onRefresh();
    }

    private void toTeamDescActivity(Integer position) {
        Intent toTeamDesc = new Intent(getContext(), TeamDescActivity.class);
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_TEAM_ID,
                joinTeamListAdapter.getItem(position).getId());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_TEAM_NAME,
                joinTeamListAdapter.getItem(position).getName());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_CREATOR_ID,
                joinTeamListAdapter.getItem(position).getUserId());
        toTeamDesc.putExtra(TeamDescActivity.EXTRA_COMPETITION_NAME,
                joinTeamListAdapter.getItem(position).getCompetitionName());
        startActivity(toTeamDesc);
    }

    @Override
    public void onRefresh() {
        presenter.refresh();
    }

    @Override
    public void onLoadMore() {
        page++;
        presenter.loadMore(page);
    }

    @Override
    public void showNoData() {
        recyclerView.showEmpty();
        joinTeamListAdapter.clear();
    }

    @Override
    public void clearAdapter() {
        joinTeamListAdapter.clear();
    }

    @Override
    public void setNoMoreData() {
        joinTeamListAdapter.stopMore();
    }

    @Override
    public void showData(List<Team> teams) {
        joinTeamListAdapter.addAll(teams);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initPage() {
        page = 1;
    }
}
