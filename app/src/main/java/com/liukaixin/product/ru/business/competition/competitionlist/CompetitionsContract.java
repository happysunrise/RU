package com.liukaixin.product.ru.business.competition.competitionlist;

import android.content.Context;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Competition;
import com.liukaixin.product.ru.enumerate.CompetitionTypeEnum;

import java.io.Serializable;
import java.util.List;

/**
 * Created by liukaixin on 16/8/29.
 */

public interface CompetitionsContract {

    interface View extends BaseView<Presenter> {

        void showError(String msg);

        void showData(List<Competition> competitions);

        void showNoData();

        void clearAdapter();

        void initPage();

        void setNoMoreData();

        Context getContext();
    }

    interface Presenter extends BasePresenter {

        void getReleasedCompetitions(Integer page, Integer rows);

        void getReleasedCompetitionsByType(Integer page,
                                           Integer rows,
                                           String type);

        void refresh();

        void loadMore(Integer page);

        void setFiltering(CompetitionTypeEnum competitionTypeEnum);

        Serializable getFiltering();

        void getData(Integer page);
    }

}
