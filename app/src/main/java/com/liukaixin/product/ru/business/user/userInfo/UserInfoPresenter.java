package com.liukaixin.product.ru.business.user.userInfo;

import android.support.annotation.NonNull;

import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.repositories.UserRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/20.
 */

public class UserInfoPresenter implements UserInfoContract.Presenter {

    @NonNull
    private final UserRepository userRepository;

    @NonNull
    private final UserInfoContract.View userInfoView;

    public UserInfoPresenter(@NonNull UserRepository userRepository,
                             @NonNull UserInfoContract.View userInfoFragment) {
        this.userRepository = checkNotNull(userRepository);
        this.userInfoView = checkNotNull(userInfoFragment);

        this.userInfoView.setPresenter(this);

    }

    @Override
    public void getUserInfoById(Integer userId) {
        userInfoView.showLoading();
        userRepository.getUserInfoById(userId, new CallBackListener<User>() {
            @Override
            public void onCompleted() {
                userInfoView.hideLoading();
            }

            @Override
            public void onError(String msg) {
                userInfoView.showError(msg);
            }

            @Override
            public void onNext(User user) {
                userInfoView.showData(user);
            }
        });
    }
}
