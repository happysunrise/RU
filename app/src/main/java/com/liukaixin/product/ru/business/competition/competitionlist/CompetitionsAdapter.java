package com.liukaixin.product.ru.business.competition.competitionlist;

import android.content.Context;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.data.model.Competition;

/**
 * Created by liukaixin on 16/8/29.
 */
public class CompetitionsAdapter extends
        RecyclerArrayAdapter<Competition> {

    public CompetitionsAdapter(Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new CompetitionViewHolder(parent);
    }
}
