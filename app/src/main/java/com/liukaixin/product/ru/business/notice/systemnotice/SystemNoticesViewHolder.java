package com.liukaixin.product.ru.business.notice.systemnotice;

import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Notice;

/**
 * Created by Jacob on 2016/9/20.
 */
public class SystemNoticesViewHolder extends BaseViewHolder<Notice> {

    private TextView titleText;
    private TextView infoText;
    private TextView createTimeText;

    public SystemNoticesViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_system_notice);
        titleText = $(R.id.title_text);
        infoText = $(R.id.info_text);
        createTimeText = $(R.id.create_time_text);
    }

    @Override
    public void setData(Notice notice) {
        titleText.setText(notice.getTitle());
        infoText.setText(notice.getInfo());
        createTimeText.setText(notice.getCreateTime());
    }
}
