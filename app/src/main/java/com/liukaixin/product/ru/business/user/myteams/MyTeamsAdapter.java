package com.liukaixin.product.ru.business.user.myteams;



import android.content.Context;
import android.view.ViewGroup;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.data.model.Team;
import com.liukaixin.product.ru.data.model.UserTeam;

/**
 * Created by Jacob on 2016/9/15.
 */
public class MyTeamsAdapter extends RecyclerArrayAdapter<UserTeam> {

    public MyTeamsAdapter (Context context) {
        super(context);
    }

    @Override
    public BaseViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyTeamsViewHolder(parent);
    }
}
