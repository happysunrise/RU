package com.liukaixin.product.ru.util;

import android.support.annotation.NonNull;

import com.orhanobut.logger.Logger;
import com.sun.crypto.provider.SunJCE;

import java.security.Provider;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.Integer.toHexString;

/**
 * Created by liukaixin on 16/9/26.
 */

public class EncryptUtil {

    private static final String ALGORITHM = "DESEDE";

    private static final byte[] KEY_BYTES = {0x11, 0x22, 0x4F, 0x58, (byte) 0x88, 0x10,
            0x40, 0x38, 0x28, 0x25, 0x79, 0x51, (byte) 0xCB, (byte) 0xDD, 0x55,
            0x66, 0x77, 0x29, 0x74, (byte) 0x98, 0x30, 0x40, 0x36, (byte) 0xE2};

    private EncryptUtil() {
        throw new IllegalAccessError("Utility class");
    }

    private static byte[] encryptMode(byte[] keybyte, byte[] src) {
        try {
            //  密钥
            SecretKey deskey = new SecretKeySpec(keybyte, ALGORITHM);

            // 加密
            Cipher c1 = Cipher.getInstance(ALGORITHM);
            c1.init(Cipher.ENCRYPT_MODE, deskey);
            return c1.doFinal(src);
        } catch (java.security.NoSuchAlgorithmException e1) {
            Logger.e("加密工具NoSuchAlgorithmException异常", e1);
        } catch (javax.crypto.NoSuchPaddingException e2) {
            Logger.e("加密工具NoSuchPaddingException异常", e2);
        } catch (java.lang.Exception e3) {
            Logger.e("加密工具java.lang.Exception异常", e3);
        }
        return new byte[10];
    }

    private static byte[] decryptMode(byte[] keybyte, byte[] src) {
        try {
            //生成密钥
            SecretKey deskey = new SecretKeySpec(keybyte, ALGORITHM);

            //解密
            Cipher c1 = Cipher.getInstance(ALGORITHM);
            c1.init(Cipher.DECRYPT_MODE, deskey);
            return c1.doFinal(src);
        } catch (java.security.NoSuchAlgorithmException e1) {
            Logger.e("加密工具NoSuchAlgorithmException异常", e1);
        } catch (javax.crypto.NoSuchPaddingException e2) {
            Logger.e("加密工具NoSuchPaddingException异常", e2);
        } catch (java.lang.Exception e3) {
            Logger.e("加密工具java.lang.Exception异常", e3);
        }
        return new byte[10];
    }

    private static String byte2hex(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp;

        for (int n = 0; n < b.length; n++) {
            stmp = toHexString(b[n] & 0XFF);
            if (stmp.length() == 1) {
                hs.append("0").append(stmp);
            } else {
                hs.append(stmp);
            }
            if (n < b.length - 1) {
                hs.append(":");
            }
        }
        return hs.toString().toUpperCase();
    }

    /**
     * 加密.
     *
     * @param originalText 原文
     * @return 加密结果
     */
    public static String encrypt(String originalText) {
        Security.addProvider(new SunJCE());
        return byte2hex(encryptMode(KEY_BYTES, originalText.getBytes()));
    }

    /**
     * 解密.
     *
     * @param encryptText 加密的文本
     * @return 解密后的结果
     */
    public static String decrypt(@NonNull String encryptText) {
        Security.addProvider(new SunJCE());
        byte[] decryption = decryptMode(KEY_BYTES, encryptText.getBytes());

        checkNotNull(decryption);

        return new String(decryption);
    }
}
