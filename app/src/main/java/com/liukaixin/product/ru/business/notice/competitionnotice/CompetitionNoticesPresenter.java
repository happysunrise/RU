package com.liukaixin.product.ru.business.notice.competitionnotice;

import android.os.Handler;
import android.support.annotation.NonNull;

import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Notice;
import com.liukaixin.product.ru.data.repositories.NoticeRepository;
import com.liukaixin.product.ru.enumerate.NoticeTypeEnum;
import com.liukaixin.product.ru.util.CheckUtil;

import java.util.List;

/**
 * Created by Jacob on 2016/9/20.
 */
public class CompetitionNoticesPresenter implements CompetitionNoticesContract.Presenter {

    @NonNull
    private final NoticeRepository noticeRepository;

    private final CompetitionNoticesContract.View competitionView;

    private Handler handler = new Handler();

    public CompetitionNoticesPresenter(NoticeRepository noticeRepository,
                                       CompetitionNoticesContract.View competitionView) {
        this.noticeRepository = noticeRepository;
        this.competitionView = competitionView;

        this.competitionView.setPresenter(this);
    }

    @Override
    public void refresh(Integer categoryId) {
        competitionView.initPage();
        // 获取第一页的十条数据
        getCompetitionNoticeByUserId(1, 10);

    }

    @Override
    public void loadMore(Integer page, Integer categoryId) {
        // 获取第一页的十条数据
        getCompetitionNoticeByUserId(page, 10);
    }

    private void getCompetitionNoticeByUserId(Integer page, Integer rows) {
        if (CheckUtil.haveNetWork(competitionView.getContext())) {
            noticeRepository.getNoticeById(page, rows,
                    NoticeTypeEnum.COMPOETITION_NOTICE.getTypeCode(),
                    new CallBackListener<List<Notice>>() {
                        @Override
                        public void onCompleted() {
                            //do nothing
                        }

                        @Override
                        public void onError(String msg) {
                            competitionView.showError(msg);
                        }

                        @Override
                        public void onNext(List<Notice> notices) {
                            handler.postDelayed(() -> {
                                if (page == 1 && notices.isEmpty()) {
                                    competitionView.showNoData();
                                    return;
                                }
                                if (page == 1) {
                                    competitionView.clearAdapter();
                                }
                                if (notices.size() % 10 > 0
                                        || (page != 1 && notices.isEmpty())) {
                                    competitionView.setNoMoreData();
                                }
                                competitionView.showData(notices);
                            }, 1000);
                        }
                    });
        }
    }


}
