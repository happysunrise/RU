package com.liukaixin.product.ru.enumerate;

/**
 * Created by liukaixin on 16/9/21.
 */

public enum ResultTypeEnum {

    FAILED(0, "失败"),
    SUCCESS(1, "成功"),
    NO_SUCH_USER(2, "用户不存在"),
    UN_CORRECT_PWD(3, "用户名或密码错误"),
    DUPLICATE_USER_NAME(4, "用户名已存在"),
    WRONG_USER_COUNT(5, "获取用户数量异常"),
    DUPLICATE_COMPETITION_NAME(6, "竞赛名重复");

    private final Integer value;
    private final String description;

    ResultTypeEnum(Integer value, String description) {
        this.value = value;
        this.description = description;
    }

    public Integer getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

}
