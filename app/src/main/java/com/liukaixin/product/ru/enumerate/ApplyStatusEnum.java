package com.liukaixin.product.ru.enumerate;

/**
 * Created by Jj on 2016/9/22.
 */
public enum  ApplyStatusEnum {

    NOT_CHECK(1, "未审核"),
    AGREE(2, "同意"),
    REJECT(3, "拒绝");

    private final Integer typeCode;
    private final String description;

    ApplyStatusEnum(Integer typeCode, String description) {
        this.typeCode = typeCode;
        this.description = description;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public String getDescription() {
        return description;
    }

}
