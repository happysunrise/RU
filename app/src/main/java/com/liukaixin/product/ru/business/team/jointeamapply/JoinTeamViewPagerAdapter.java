package com.liukaixin.product.ru.business.team.jointeamapply;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by liukaixin on 16/9/19.
 */

public class JoinTeamViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;

    public JoinTeamViewPagerAdapter(FragmentManager supportFragmentManager,
                            List<Fragment> fragments) {
        super(supportFragmentManager);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

}
