package com.liukaixin.product.ru.business.team.createteam;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.liukaixin.product.ru.AppController;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.user.editUser.EditUserActivity;
import com.liukaixin.product.ru.data.model.Team;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.util.LVCircularSmile;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Jacob on 2016/9/20.
 */
public class CreateTeamFragment extends Fragment
        implements CreateTeamContract.View {

    @NonNull
    private static final String ARGUMENT_COMPETITION_ID = "COMPETITION_ID";
    @NonNull
    private static final String ARGUMENT_COMPETITION_NAME = "COMPETITION_NAME";

    private CreateTeamContract.Presenter presenter;

    private EditText nameEdit;
    private EditText introEdit;
    private EditText requireEdit;
//    private EditText mottoEdit;
    private EditText numberEdit;
    private EditText creatorIntroEdit;

    private TextInputLayout teamNameTil, teamIntroTil, teamRequireTil,
            teammateNumberTil, creatorIntroTil;

    LVCircularSmile smileLoading;

    public CreateTeamFragment() {

    }

    public static CreateTeamFragment newInstance(@Nullable Integer competitionId, String competitionName) {
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_COMPETITION_ID, competitionId);
        arguments.putString(ARGUMENT_COMPETITION_NAME, competitionName);
        CreateTeamFragment fragment = new CreateTeamFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void setPresenter(@NonNull CreateTeamContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_create_team, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);

        Button finishBtn = (Button) view.findViewById(R.id.finish_btn);
        finishBtn.setOnClickListener(v -> {
            if (nameEdit.getText().toString().trim().isEmpty()) {
                teamNameTil.setErrorEnabled(true);
                teamNameTil.setError("此项不能为空");
            } else if (introEdit.getText().toString().trim().isEmpty()) {
                teamIntroTil.setErrorEnabled(true);
                teamIntroTil.setError("此项不能为空");
            } else if (requireEdit.getText().toString().trim().isEmpty()) {
                teamRequireTil.setErrorEnabled(true);
                teamRequireTil.setError("此项不能为空");
            }
//            else if (mottoEdit.getText().toString().trim().isEmpty()) {
//                mottoEdit.setError("此项不能为空");
//            }
            else if (numberEdit.getText().toString().trim().isEmpty()) {
                teammateNumberTil.setErrorEnabled(true);
                teammateNumberTil.setError("此项不能为空");
            } else if (creatorIntroEdit.getText().toString().trim().isEmpty()) {
                creatorIntroTil.setErrorEnabled(true);
                creatorIntroTil.setError("此项不能为空");
            } else {
                User user = JSON.parseObject(
                        AppController.getInstance().getCache().getAsString("user"), User.class);
                if (user.getSchool() == null || user.getSchool().isEmpty()) {
                    toUpdateUserInfo();
                } else if (user.getDegree() == null || user.getDegree().isEmpty()) {
                    toUpdateUserInfo();
                } else if (user.getRealName() == null || user.getRealName().isEmpty()) {
                    toUpdateUserInfo();
                } else {
                    sendCreateTeam(user);
                }
            }
        });
    }

    private void sendCreateTeam(User user) {
        Team team = new Team(nameEdit.getText().toString().trim(),
                introEdit.getText().toString().trim(),
                requireEdit.getText().toString().trim(),
                Integer.parseInt(numberEdit.getText().toString().trim()),
                creatorIntroEdit.getText().toString().trim());
        team.setCompetitionName(this.getArguments().getString(ARGUMENT_COMPETITION_NAME));
        team.setCompetitionId(this.getArguments().getInt(ARGUMENT_COMPETITION_ID));
        team.setSchool(user.getSchool());
        team.setUserId(user.getId());
        team.setUserNickName(user.getNickName());
        team.setPic(user.getPic());
        presenter.sendCreatedTeam(team);
    }

    private void toUpdateUserInfo() {
        Toast.makeText(getActivity(), "请先补充个人信息~", Toast.LENGTH_LONG).show();
        Intent toUpdateUser = new Intent(getActivity(), EditUserActivity.class);
        startActivity(toUpdateUser);
    }

    private void initView(View view) {
        nameEdit = (EditText) view.findViewById(R.id.name_edit);
        introEdit = (EditText) view.findViewById(R.id.intro_edit);
        requireEdit = (EditText) view.findViewById(R.id.require_edit);
//        mottoEdit = (EditText) view.findViewById(R.id.motto_edit);
        numberEdit = (EditText) view.findViewById(R.id.number_edit);
        creatorIntroEdit = (EditText) view.findViewById(R.id.creator_intro_edit);
        smileLoading = (LVCircularSmile) view.findViewById(R.id.lv_circularSmile);

        teamNameTil = (TextInputLayout) view.findViewById(R.id.team_name_til);
        teamIntroTil = (TextInputLayout) view.findViewById(R.id.team_intro_til);
        teamRequireTil = (TextInputLayout) view.findViewById(R.id.team_require_til);
        teammateNumberTil = (TextInputLayout) view.findViewById(R.id.teammate_number_til);
        creatorIntroTil = (TextInputLayout) view.findViewById(R.id.creator_intro_til);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        smileLoading.setVisibility(View.VISIBLE);
        smileLoading.startAnim();
    }

    @Override
    public void hideLoading() {
        smileLoading.setVisibility(View.GONE);
        smileLoading.stopAnim();
    }

    @Override
    public void toSuccessCreatedTeam(String s) {
        Toast.makeText(getActivity(), s,
                Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

}
