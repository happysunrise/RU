package com.liukaixin.product.ru.business.team.teammember;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * Created by liukaixin on 16/9/19.
 */

public class TeamMemberActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_team_memeber);

        // Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        TeamMemberFragment teamMemberFragment = (TeamMemberFragment) getSupportFragmentManager()
                .findFragmentById(R.id.content_frame);

        if (teamMemberFragment == null) {
            teamMemberFragment = TeamMemberFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    teamMemberFragment, R.id.content_frame);
        }

        // Create the presenter
        new TeamMemberPresenter(Injection.provideTeamsRepository(),
                teamMemberFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
