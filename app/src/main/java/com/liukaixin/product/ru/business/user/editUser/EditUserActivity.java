package com.liukaixin.product.ru.business.user.editUser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * Created by liukaixin on 16/9/20.
 */

public class EditUserActivity extends AppCompatActivity {

    public static final Integer REQUEST_CODE_UPDATE_USER = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        EditUserFragment editUserFragment = (EditUserFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (editUserFragment == null) {
            editUserFragment = EditUserFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), editUserFragment, R.id.content_frame);
        }

        // Create the presenter
        new EditUserPresenter(Injection.provideUserRepository(),
                editUserFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
