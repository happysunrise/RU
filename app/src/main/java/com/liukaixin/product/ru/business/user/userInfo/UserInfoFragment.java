package com.liukaixin.product.ru.business.user.userInfo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.liukaixin.product.ru.AppController;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.user.editUser.EditUserActivity;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.util.LVCircularSmile;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/20.
 */

public class UserInfoFragment extends Fragment
        implements UserInfoContract.View {

    private UserInfoContract.Presenter presenter;

    private TextView nickNameText;

    private TextView genderText;

    private TextView schoolText;

    private TextView majorText;

    private TextView realNameText;

    private TextView degreeText;

    LVCircularSmile loadingSmile;

    public static UserInfoFragment newInstance() {
        return new UserInfoFragment();
    }

    public UserInfoFragment() {
        // Required empty public constructor.
    }

    @Override
    public void setPresenter(@NonNull UserInfoContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_user_info, container, false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadingSmile = (LVCircularSmile) view.findViewById(R.id.lv_circularSmile);

        nickNameText = (TextView) view.findViewById(R.id.nick_name_text);
        realNameText = (TextView) view.findViewById(R.id.real_name_text);
        genderText = (TextView) view.findViewById(R.id.gender_text);
        schoolText = (TextView) view.findViewById(R.id.school_text);
        majorText = (TextView) view.findViewById(R.id.major_text);
        degreeText = (TextView) view.findViewById(R.id.degree_text);

        FloatingActionButton editFab = (FloatingActionButton) view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(v -> toEditActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        User user = JSON.parseObject(AppController.getInstance().getCache().getAsString("user"),
                User.class);

//        presenter.getUserInfoById(user.getId());
        showData(user);
    }

    private void toEditActivity() {
        Intent toEdit = new Intent(getActivity(), EditUserActivity.class);
        startActivity(toEdit);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(User user) {
        if (user.getNickName() != null) {
            nickNameText.setText(user.getNickName());
        }
        if (user.getGender() != null) {
            genderText.setText(user.getGender());
        }
        if (user.getSchool() != null) {
            schoolText.setText(user.getSchool());
        }
        if (user.getMajor() != null) {
            majorText.setText(user.getMajor());
        }
        if (user.getDegree() != null) {
            degreeText.setText(user.getDegree());
        }
        if (user.getRealName() != null) {
            realNameText.setText(user.getRealName());
        }
    }

    @Override
    public void showLoading() {
        loadingSmile.startAnim();
    }

    @Override
    public void hideLoading() {
        loadingSmile.stopAnim();
    }
}
