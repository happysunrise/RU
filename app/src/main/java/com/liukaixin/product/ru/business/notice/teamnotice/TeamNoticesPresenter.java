package com.liukaixin.product.ru.business.notice.teamnotice;

import android.os.Handler;
import android.support.annotation.NonNull;

import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Notice;
import com.liukaixin.product.ru.data.repositories.NoticeRepository;
import com.liukaixin.product.ru.enumerate.NoticeTypeEnum;
import com.liukaixin.product.ru.util.CheckUtil;
import com.orhanobut.logger.Logger;

import java.util.List;

/**
 * Created by Jacob on 2016/9/20.
 */
public class TeamNoticesPresenter implements TeamNoticesContract.Presenter {

    @NonNull
    private final NoticeRepository noticeRepository;

    private final TeamNoticesContract.View teamView;

    private Handler handler = new Handler();

    public TeamNoticesPresenter(NoticeRepository noticeRepository,
                                TeamNoticesContract.View teamView) {
        this.noticeRepository = noticeRepository;
        this.teamView = teamView;

        this.teamView.setPresenter(this);
    }

    @Override
    public void refresh(Integer categoryId) {
        teamView.initPage();
        // 获取第一页的十条数据
        getApplyByUserId(1, 10);
    }

    @Override
    public void loadMore(Integer page, Integer categoryId) {
        // 获取第一页的十条数据
        getApplyByUserId(page, 10);

    }

    private void getApplyByUserId(Integer page, Integer rows) {
        if (CheckUtil.haveNetWork(teamView.getContext())) {
            noticeRepository.getNoticeById(page, rows,
                    NoticeTypeEnum.TEAM_NOTICE.getTypeCode(),
                    new CallBackListener<List<Notice>>() {
                        @Override
                        public void onCompleted() {
                            //do nothing
                        }

                        @Override
                        public void onError(String msg) {
                            teamView.showError(msg);
                        }

                        @Override
                        public void onNext(List<Notice> notices) {
                            handler.postDelayed(() -> {
                                if (page == 1 && notices.isEmpty()) {
                                    teamView.showNoData();
                                    return;
                                }
                                if (page == 1) {
                                    teamView.clearAdapter();
                                }
                                if (notices.size() % 10 > 0
                                        || (page != 1 && notices.isEmpty())) {
                                    teamView.setNoMoreData();
                                }
                                teamView.showData(notices);
                            }, 1000);
                        }
                    });
        }
    }

}
