package com.liukaixin.product.ru;

/**
 * Created by liukaixin on 16/8/29.
 */

public interface BaseView<T> {

    void setPresenter(T presenter);

}
