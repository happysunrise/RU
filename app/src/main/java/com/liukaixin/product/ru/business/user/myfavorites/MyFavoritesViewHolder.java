package com.liukaixin.product.ru.business.user.myfavorites;

import android.support.v7.widget.CardView;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.enumerate.FavoriteTypeEnum;

/**
 * Created by Jacob on 2016/9/16.
 */
public class MyFavoritesViewHolder extends BaseViewHolder<Favorite>{

    private TextView nameText;
    private TextView createTimeText;
    private CardView cardView;

    public MyFavoritesViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_my_favorite);
        nameText = $(R.id.name_text);
        createTimeText = $(R.id.create_time_text);
        cardView = $(R.id.card_view);
    }

    @Override
    public void setData(Favorite favorite) {
        nameText.setText(favorite.getStarName());
        createTimeText.setText(String.valueOf(favorite.getCreateTime()));
        if (favorite.getCategory().equals(FavoriteTypeEnum.COMPETITION.getTypeCode())) {
            cardView.setCardBackgroundColor(0x7f00BCD4);
        } else {
            cardView.setCardBackgroundColor(0x7f03A9F4);
        }
    }
}
