package com.liukaixin.product.ru.business.user.editUser;

import android.support.annotation.NonNull;

import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.User;
import com.liukaixin.product.ru.data.repositories.UserRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/20.
 */

public class EditUserPresenter implements EditUserContract.Presenter {

    @NonNull
    private final UserRepository userRepository;

    @NonNull
    private final EditUserContract.View editUserView;

    public EditUserPresenter(@NonNull UserRepository userRepository,
                             @NonNull EditUserContract.View editUserView) {
        this.userRepository = checkNotNull(userRepository);
        this.editUserView = checkNotNull(editUserView);

        this.editUserView.setPresenter(this);
    }

    @Override
    public void updateUser(User user) {
        editUserView.showLoading();
        userRepository.updateUser(user, new CallBackListener<String>() {
            @Override
            public void onCompleted() {
                editUserView.hideLoading();
            }

            @Override
            public void onError(String msg) {
                editUserView.showError(msg);
            }

            @Override
            public void onNext(String result) {
                editUserView.showResult(result);
            }
        });
    }
}
