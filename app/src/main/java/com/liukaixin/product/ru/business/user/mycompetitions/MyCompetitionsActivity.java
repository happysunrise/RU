package com.liukaixin.product.ru.business.user.mycompetitions;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;
import com.orhanobut.logger.Logger;

/**
 * Created by Jacob on 2016/9/14.
 */
public class MyCompetitionsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_competitions);

        // Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        MyCompetitionsFragment myCompetitionsFragment = (MyCompetitionsFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if(myCompetitionsFragment == null) {
            myCompetitionsFragment = MyCompetitionsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    myCompetitionsFragment,R.id.content_frame);
        }

        // Create the presenter
        new MyCompetitionsPresenter(Injection.provideCompetitionsRepository(),
                myCompetitionsFragment);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
