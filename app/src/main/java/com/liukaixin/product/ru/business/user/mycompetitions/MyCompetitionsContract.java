package com.liukaixin.product.ru.business.user.mycompetitions;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;
import com.liukaixin.product.ru.data.model.Competition;

import java.util.List;

/**
 * Created by Jacob on 2016/9/14.
 */
public interface MyCompetitionsContract {
    interface View extends BaseView<Presenter>{

        void initPage();

        void showError(String msg);

        void showNoData();

        void clearAdapter();

        void setNoMoreData();

        void showData(List<Competition> competition);
    }

    interface Presenter extends BasePresenter{
        void refresh();

        void loadMore(Integer page);
    }
}
