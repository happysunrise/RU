package com.liukaixin.product.ru.business.user.register;

import android.content.Context;

import com.liukaixin.product.ru.BasePresenter;
import com.liukaixin.product.ru.BaseView;

/**
 * 注册约定.
 *
 * Created by liukaixin on 16/9/7.
 */

public class RegisterContract {

    interface View extends BaseView<Presenter> {
        String getUsername();

        String getPassword();

        void showLoading();

        void hideLoading();

        void showError(String msg);

        void showRegisterSuccessActivity(String msg);

        String getSchool();

        String getDegree();

        void emptyUsername();

        void emptyPassword();

        void emptySchool();

        void userNameFormatDismatch();

        void pwdNotCorrected();

        Context getActivity();
    }

    interface Presenter extends BasePresenter {
        void register();
    }
}