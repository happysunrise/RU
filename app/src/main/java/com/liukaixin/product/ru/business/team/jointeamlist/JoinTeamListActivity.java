package com.liukaixin.product.ru.business.team.jointeamlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.liukaixin.product.ru.Injection;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.util.ActivityUtils;

/**
 * Created by Jacob on 2016/9/18.
 */
public class JoinTeamListActivity extends AppCompatActivity {

    public static final String EXTRA_COMPETITION_ID = "COMPETITION_ID";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_team_list);

        // Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        // Get the requested competition id
        Integer competitionId = getIntent().getIntExtra(EXTRA_COMPETITION_ID, 1);

        JoinTeamListFragment joinTeamListFragment = (JoinTeamListFragment)
                getSupportFragmentManager().findFragmentById(R.id.content_frame);

        if (joinTeamListFragment == null) {
            joinTeamListFragment = JoinTeamListFragment.newInstance(competitionId);
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    joinTeamListFragment, R.id.content_frame);
        }

        // Create the presenter
        new JoinTeamListPresenter(competitionId, Injection.provideTeamsRepository(),
                joinTeamListFragment);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
