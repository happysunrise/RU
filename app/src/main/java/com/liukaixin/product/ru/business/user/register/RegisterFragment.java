package com.liukaixin.product.ru.business.user.register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.home.HomeActivity;
import com.liukaixin.product.ru.util.LVCircularSmile;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/7.
 */

public class RegisterFragment extends Fragment
        implements RegisterContract.View, View.OnClickListener {

    private RegisterContract.Presenter presenter;

    private EditText userNameEdit;

    private EditText passwordEdit;

    private EditText schoolEdit;

    private LVCircularSmile loadingSmile;

    private RadioGroup radioGroup;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    public RegisterFragment() {
        // Required empty public constructor.
    }

    @Override
    public void setPresenter(@NonNull RegisterContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_register, container, false);

        userNameEdit = (EditText) root.findViewById(R.id.user_name_edit);
        passwordEdit = (EditText) root.findViewById(R.id.password_edit);
        schoolEdit = (EditText) root.findViewById(R.id.school_edit);

        radioGroup = (RadioGroup) root.findViewById(R.id.degree_rg);
        Button okButton = (Button) root.findViewById(R.id.ok_button);
        loadingSmile = (LVCircularSmile) root.findViewById(R.id.lv_circularSmile);
        loadingSmile.setVisibility(View.INVISIBLE);

        setHasOptionsMenu(true);
        setRetainInstance(true);
        okButton.setOnClickListener(this);
        return root;
    }

    @Override
    public String getUsername() {
        return userNameEdit.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return passwordEdit.getText().toString().trim();
    }

    @Override
    public String getSchool() {
        return schoolEdit.getText().toString().trim();
    }

    @Override
    public String getDegree() {
        if (radioGroup.getCheckedRadioButtonId() == R.id.s_rb) {
            return "硕士";
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.b_rb) {
            return "本科";
        } else if (radioGroup.getCheckedRadioButtonId() == R.id.z_rb) {
            return "专科";
        } else {
            return "博士";
        }
    }

    @Override
    public void emptyUsername() {
        userNameEdit.setError("用户名不能为空");
    }

    @Override
    public void emptyPassword() {
        passwordEdit.setError("密码不能为空");
    }

    @Override
    public void emptySchool() {
        schoolEdit.setError("学校不能为空");
    }

    @Override
    public void userNameFormatDismatch() {
        userNameEdit.setError("请输入正确的邮箱");
    }

    @Override
    public void pwdNotCorrected() {
        passwordEdit.setError("密码只能是数字/字母");
    }

    @Override
    public void showLoading() {
        loadingSmile.setVisibility(View.VISIBLE);
        loadingSmile.startAnim();
    }

    @Override
    public void hideLoading() {
        loadingSmile.setVisibility(View.GONE);
        loadingSmile.stopAnim();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showRegisterSuccessActivity(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override
    public void onClick(View view) {
        presenter.register();
    }
}
