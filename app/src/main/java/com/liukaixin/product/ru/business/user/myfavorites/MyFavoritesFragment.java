package com.liukaixin.product.ru.business.user.myfavorites;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.liukaixin.product.ru.R;
import com.liukaixin.product.ru.business.competition.competitiondesc.CompetitionDescActivity;
import com.liukaixin.product.ru.business.team.teamdesc.TeamDescActivity;
import com.liukaixin.product.ru.data.model.Favorite;
import com.liukaixin.product.ru.enumerate.FavoriteTypeEnum;
import com.liukaixin.product.ru.util.CheckUtil;
import com.orhanobut.logger.Logger;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Jacob on 2016/9/16.
 */
public class MyFavoritesFragment extends Fragment implements MyFavoritesContract.View,
        RecyclerArrayAdapter.OnLoadMoreListener, SwipeRefreshLayout.OnRefreshListener{

    private MyFavoritesContract.Presenter presenter;

    private MyFavoritesAdapter myFavoritesAdapter;

    private EasyRecyclerView recyclerView;

    private Integer page = 1;

    public MyFavoritesFragment() {
        // Requires empty public constructor
    }

    public static MyFavoritesFragment newInstance() {
        return new MyFavoritesFragment();
    }

    @Override
    public void setPresenter(MyFavoritesContract.Presenter presenter) {
        this.presenter = checkNotNull(presenter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_favorites,container,false);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        return root;
    }

    @Override
    public void onViewCreated(View view,@Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        myFavoritesAdapter = new MyFavoritesAdapter(getActivity());
        // Set up my competitions view
        recyclerView =
                (EasyRecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapterWithProgress(myFavoritesAdapter);
        recyclerView.setRefreshListener(this);

        myFavoritesAdapter.setMore(R.layout.view_more,this);
        myFavoritesAdapter.setNoMore(R.layout.view_no_more);
        myFavoritesAdapter.setOnItemClickListener(
                this::toCollectionsActivity);

        myFavoritesAdapter.setError(R.layout.view_error)
                .setOnClickListener(
                        v -> myFavoritesAdapter.resumeMore());
    }

    @Override
    public void onResume() {
        super.onResume();
        onRefresh();

    }

    private void toCollectionsActivity(Integer position) {
        if (myFavoritesAdapter.getItem(position).getCategory()
                .equals(FavoriteTypeEnum.COMPETITION.getTypeCode())) {
            Intent toCompetitionDesc = new Intent(getActivity(), CompetitionDescActivity.class);
            toCompetitionDesc.putExtra(CompetitionDescActivity.EXTRA_COMPETITION_ID,
                    myFavoritesAdapter.getItem(position).getStarId());
            toCompetitionDesc.putExtra(CompetitionDescActivity.EXTRA_COMPETITION_NAME,
                    myFavoritesAdapter.getItem(position).getStarName());
            startActivity(toCompetitionDesc);
        } else {
            Intent toTeamDesc = new Intent(getContext(), TeamDescActivity.class);
            toTeamDesc.putExtra(TeamDescActivity.EXTRA_TEAM_ID,
                    myFavoritesAdapter.getItem(position).getStarId());
            toTeamDesc.putExtra(TeamDescActivity.EXTRA_TEAM_NAME,
                    myFavoritesAdapter.getItem(position).getStarName());

            startActivity(toTeamDesc);
        }
    }

    @Override
    public void onRefresh() {
        presenter.refresh();
    }

    @Override
    public void onLoadMore() {
        page++;
        presenter.loadMore(page);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(List<Favorite> favorites) {
        recyclerView.setVisibility(View.VISIBLE);
        myFavoritesAdapter.addAll(favorites);
    }

    @Override
    public void showNoData() {
        recyclerView.showEmpty();
        myFavoritesAdapter.clear();
    }

    @Override
    public void clearAdapter() {
        myFavoritesAdapter.clear();
    }

    @Override
    public void initPage() {
        page = 1;
    }

    @Override
    public void setNoMoreData() {
        myFavoritesAdapter.stopMore();
    }

}
