package com.liukaixin.product.ru.business.team.jointeamapply;

import android.os.Handler;
import android.support.annotation.Nullable;

import com.liukaixin.product.ru.business.team.teamlist.TeamsContract;
import com.liukaixin.product.ru.data.CallBackListener;
import com.liukaixin.product.ru.data.model.Apply;
import com.liukaixin.product.ru.data.repositories.CompetitionsRepository;
import com.liukaixin.product.ru.data.repositories.TeamsRepository;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by liukaixin on 16/9/19.
 */

public class JoinTeamApplyAbilityPresenter
        implements JoinTeamApplyContract.Presenter{

    private final TeamsRepository teamsRepository;

    private final JoinTeamApplyContract.View joinTeamApplyView;

    @Nullable
    private Integer teamId;

    public JoinTeamApplyAbilityPresenter(Integer teamId, TeamsRepository teamsRepository,
                                         JoinTeamApplyAbilityFragment joinTeamApplyView) {
        this.teamId = teamId;
        this.teamsRepository = checkNotNull(teamsRepository);
        this.joinTeamApplyView = checkNotNull(joinTeamApplyView);
        this.joinTeamApplyView.setPresenter(this);
    }

    /**
     * 发送"我的特长"到服务器.
     *
     * @param apply 特长
     */
    @Override
    public void sendAbility(Apply apply) {
        joinTeamApplyView.showLoading();
        teamsRepository.sendAbility(apply,
                new CallBackListener<String>() {
            @Override
            public void onCompleted() {
                joinTeamApplyView.hideLoading();
            }

            @Override
            public void onError(String msg) {
                joinTeamApplyView.showError(msg);
            }

            @Override
            public void onNext(String s) {
                joinTeamApplyView.toSuccessActivity(s);
            }
        });
    }
}
