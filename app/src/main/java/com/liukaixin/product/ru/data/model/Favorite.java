package com.liukaixin.product.ru.data.model;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * Created by Jacob on 2016/9/16.
 */
public class Favorite {

    /**
     *  收藏名
     */
    private String starName;

    /**
     * 创建日期
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String createTime;

    /**
     *  收藏Id
     */
    private Integer starId;

    /**
     *  类别
     */
    private Integer category;

    /**
     * 收藏者.
     */
    private Integer userId;

    public Favorite() {

    }

    public String getStarName() {
        return starName;
    }

    public void setStarName(String starName) {
        this.starName = starName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getStarId() {
        return starId;
    }

    public void setStarId(Integer starId) {
        this.starId = starId;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Favorite{" +
                "starName='" + starName + '\'' +
                ", createTime='" + createTime + '\'' +
                ", starId=" + starId +
                ", category=" + category +
                '}';
    }
}
